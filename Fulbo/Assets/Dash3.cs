using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class Dash3 : MonoBehaviour
{
    public static bool FueAgarrado;
    public float enfriamientoCoso;
    public Collider coso;
    public MeshRenderer meshRender;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        

        if (enfriamientoCoso > 4)
        {
            FueAgarrado = false;
            enfriamientoCoso = 0;

        }


        if (FueAgarrado)
        {
            enfriamientoCoso += Time.deltaTime;
            //this.gameObject.SetActive(false);
            coso.enabled = false;
            meshRender.enabled = false;
        }
        else
        {
            coso.enabled = true;
            meshRender.enabled = true;
            //this.gameObject.SetActive(true);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            TutorialController.AgarroDash = true;
            FueAgarrado = true;
            Dashing.DashesDisponibles = 3;
        }

        if (other.gameObject.name == "Player 2")
        {
            FueAgarrado = true;
            DashingP2.DashesDisponibles = 3;
        }

        if (other.gameObject.name == "Player 3")
        {
            FueAgarrado = true;
            DashingP3.DashesDisponibles = 3;
        }

        if (other.gameObject.name == "Player 4")
        {
            FueAgarrado = true;
            DashingP4.DashesDisponibles = 3;
        }

    }
}
