using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialController : MonoBehaviour
{
    public GameObject pBienvenida;

    //public static bool TutoMoverCompleto;
    public static int contLlevarseLaBola;
    public static bool AgarroDash;
    public static bool DasheoAlRival;
    public static bool StuneeConUnParry;

    public GameObject pLLevarseBola;
    public GameObject Arco1;
    public GameObject pComoPatear;
    public GameObject pGol;
    public GameObject pGol2;
    public GameObject pGol3ObstaculosDiagonal;
    public GameObject Obstaculos3TiroEnDiagonal;
    public GameObject pPicarlaHacerGol;
    public GameObject ObstaculosPicarla;
    public GameObject pPicarlaHacerGol2;
    public GameObject pBarrer1;
    public GameObject pBarrer2;
    public GameObject rival2;
    public GameObject CirculitoDash;
    public GameObject ContDash;
    public GameObject pBarrer3;
    public GameObject pBarrer4;

    void Start()
    {
        EntraBola.GolesAmarillo = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(AgarroDash);
        ComienzaElTutorial();

        if (contLlevarseLaBola>3)
        {
            pLLevarseBola.SetActive(false);
            PatearAlArco();
        }
    }


    public void ComienzaElTutorial()
    {
        if (Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            pBienvenida.SetActive(false);
            TutoMoverse();
        }
    }

    public void TutoMoverse()
    {
        pLLevarseBola.SetActive(true);

     
    }

    public void PatearAlArco()
    {
        //aparece arco
        //aparece panel como patear
   

        switch (EntraBola.GolesAmarillo)
        {
            case 0:
                {

                    Arco1.SetActive(true);
                    pComoPatear.SetActive(true);
                    break;
                }
            case 1:
                {
                    pComoPatear.SetActive(false);
                    pGol.SetActive(true);
                    break;
                }
            case 2:
                {
                    pGol.SetActive(false);
                    pGol2.SetActive(true);
                    break;
                }
            case 3:
                {
                    pGol2.SetActive(false);
                    pGol3ObstaculosDiagonal.SetActive(true);
                    Obstaculos3TiroEnDiagonal.SetActive(true);
                    break;
                }
            case 4:
                {
                    pGol3ObstaculosDiagonal.SetActive(false);
                    Obstaculos3TiroEnDiagonal.SetActive(false);
                    pComoPatear.SetActive(false);
                    pPicarlaHacerGol.SetActive(true);
                    ObstaculosPicarla.SetActive(true);
                    break;
                }
            case 5:
                {
                    
                    pPicarlaHacerGol.SetActive(false);
                    pPicarlaHacerGol2.SetActive(true);
                    break;
                }
            case 6:
                {

                    Arco1.SetActive(false);
                    pPicarlaHacerGol2.SetActive(false);
                    ObstaculosPicarla.SetActive(false);
                    Dashear();

                    break;
                }
        }

    }

    public void Dashear()
    {
        pBarrer1.SetActive(true);
        ContDash.SetActive(true);
        CirculitoDash.SetActive(true);

        if (AgarroDash)
        {
            pBarrer1.SetActive(false);
            pBarrer2.SetActive(true);
            rival2.SetActive(true);

            if (DasheoAlRival)
            {
                pBarrer2.SetActive(false);
                pBarrer3.SetActive(true);



                //hacer que el rival dashee solo



                if (StuneeConUnParry)
                {
                    pBarrer4.SetActive(true);
                    pBarrer3.SetActive(false);

                    if (Input.GetKeyDown(KeyCode.Joystick1Button0))
                    {
                        SceneManager.LoadScene(0);
                    }

                }
            }
        }

    }


}
