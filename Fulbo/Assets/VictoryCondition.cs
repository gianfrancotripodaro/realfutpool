using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryCondition : MonoBehaviour
{
    public static int contBolasAmarillas;
    public static int contBolasVioletas;

    public static float desactivarParticulas;
    public static bool entra;

    public static bool EntroNegra;

    public GameObject GanaAmarillo;
    public GameObject GanaVioleta;

    public GameObject Turnomarillo;
    public GameObject TurnoVioleta;

    //public static bool FueAgarrado;

    public static int PateaTeam;

    public static bool patadasTeamAmarillox2;
    public static bool patadasTeamVioletax2;

    public static int ContTimesPatadasAmarillo;
    public static int ContTimesPatadasVioloeta;


    public GameObject bA1;
    public GameObject bA2;
    public GameObject bA3;
    public GameObject bA4;
    public GameObject bA5;

    public GameObject bV1;
    public GameObject bV2;
    public GameObject bV3;
    public GameObject bV4;
    public GameObject bV5;


    void Start()
    {
        EntraBola.GolesVioleta = 0;
        EntraBola.GolesAmarillo = 0;
    }

    // Update is called once per frame
    void Update()
    {

        if (EntraBola.GolesVioleta >= 5)
        {
            GanaVioleta.SetActive(true);
            if (Input.GetKeyDown(KeyCode.JoystickButton0) && SceneManager.GetSceneByName("Street") == SceneManager.GetActiveScene())
            {
                SceneManager.LoadScene(0);
            }
        }
        else if (EntraBola.GolesAmarillo >= 5)
        {
          GanaAmarillo.SetActive(true);
            if (Input.GetKeyDown(KeyCode.JoystickButton0) && SceneManager.GetSceneByName("Street") == SceneManager.GetActiveScene())
            {
                SceneManager.LoadScene(0);
            }
        }


        switch (EntraBola.GolesVioleta)
        {
            case 0:
                {
                    bV1.SetActive(false);
                    bV2.SetActive(false);
                    bV3.SetActive(false);
                    bV4.SetActive(false);
                    bV5.SetActive(false);
                    break;
                }
            case 1:
                {
                    bV1.SetActive(true);
                    break;
                }
            case 2:
                {
                    bV2.SetActive(true);
                    break;
                }
            case 3:
                {
                    bV3.SetActive(true);
                    break;
                }
            case 4:
                {
                    bV4.SetActive(true);
                    break;
                }
            case 5:
                {
                    bV5.SetActive(true);
                    GanaVioleta.SetActive(true);
                    break;
                }
            //case > 5:
            //    {
            //        GanaVioleta.SetActive(true);
            //        break;
            //    }
        }

        switch (EntraBola.GolesAmarillo)
        {
            case 0:
                {
                    bA1.SetActive(false);
                    bA2.SetActive(false);
                    bA3.SetActive(false);
                    bA4.SetActive(false);
                    bA5.SetActive(false);
                    break;
                }
            case 1:
                {
                    bA1.SetActive(true);
                    break;
                }
            case 2:
                {
                    bA2.SetActive(true);
                    break;
                }
            case 3:
                {
                    bA3.SetActive(true);
                    break;
                }
            case 4:
                {
                    bA4.SetActive(true);
                    break;
                }
            case 5:
                {
                    bA5.SetActive(true);
                    GanaAmarillo.SetActive(true);
                    break;
                }
            //case > 5:
            //    {
            //        GanaAmarillo.SetActive(true);
            //        break;
            //    }
        }



        //Debug.Log("Patadas amarillas: " + ContTimesPatadasAmarillo);
        //Debug.Log("Patadas violeta: " + ContTimesPatadasVioloeta );

        if (PateaTeam == 2)
        {
            //if (patadasTeamAmarillox2)
            //{

            //}
            //else
            //{

            //}
            ContTimesPatadasVioloeta = 1;
            //pateaVioleta
            TurnoVioleta.SetActive(true);
            Turnomarillo.SetActive(false);
        }
        else if (PateaTeam == 1)
        {
            ContTimesPatadasAmarillo = 1;
            //pateaAmarillo
            Turnomarillo.SetActive(true);
            TurnoVioleta.SetActive(false);
        }


        //if (patadasTeamAmarillox2)
        //{
        //    patea 2 veces
        //    PateaTeam = 1;
        //    ContTimesPatadasAmarillo = 2;
        //    patadasTeamVioletax2 = false;
        //}
        //else if (patadasTeamVioletax2)
        //{
        //    PateaTeam = 2;
        //    patea 2 veces
        //    ContTimesPatadasVioloeta = 2;
        //    patadasTeamAmarillox2 = false;
        //}

        //if (ContTimesPatadasAmarillo < 1)
        //{
        //    PateaTeam = 2;
        //}
        //else if (ContTimesPatadasVioloeta < 1)
        //{
        //    PateaTeam = 1;
        //}

        //Debug.Log(PateaTeam);

        if (contBolasVioletas == 4)
        {
            Debug.Log("puede meter la negra");

            if (EntroNegra)
            {
                GanoTeamVioleta();
                Debug.Log("GanasteGenio");
            }

        }
        else
        {
            if (EntroNegra)
            {
                GanoTeamAmarillo();
                Debug.Log("Perdiste hermano");
            }
        }

        if (contBolasAmarillas == 4)
        {
            Debug.Log("puede meter la negra");
            if (EntroNegra)
            {
                GanoTeamAmarillo();
                Debug.Log("GanasteGenio");
            }
        }
        else
        {
            if (EntroNegra)
            {
                GanoTeamVioleta();
                Debug.Log("Perdiste hermano");
            }
        }






    }


    public void GanoTeamVioleta()
    {
        GanaVioleta.SetActive(true);
        if (Input.GetKeyDown(KeyCode.JoystickButton0) /*&& SceneManager.GetSceneByName("FutPool") == SceneManager.GetActiveScene()*/)
        {
            EntraBola.GolesVioleta = 0;
            EntraBola.GolesAmarillo = 0;
            SceneManager.LoadScene(0);

        }
    }

    public void GanoTeamAmarillo()
    {
        GanaAmarillo.SetActive(true);
        if (Input.GetKeyDown(KeyCode.JoystickButton0) /*&& SceneManager.GetSceneByName("FutPool") == SceneManager.GetActiveScene()*/)
        {
            EntraBola.GolesVioleta = 0;
            EntraBola.GolesAmarillo = 0;
            SceneManager.LoadScene(0);
        }
    }

}
