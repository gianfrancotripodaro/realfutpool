using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntraBola : MonoBehaviour
{
   

    public GameObject Particulas1;
    public GameObject Particulas2;
    public GameObject Particulas3;
    public GameObject Particulas4;
    public GameObject Particulas5;
    public GameObject Particulas6;

    //public GameObject BolaBlancaInstanciar;

    public float contInstanciarBlanca;

    public bool gol;


    public Rigidbody rbBolaBlanca;


    public static int GolesAmarillo;
    public static int GolesVioleta;


    

    //private rbBall;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        //if (gol)
        //{
        //    contInstanciarBlanca += Time.deltaTime;
        //}

        //if (contInstanciarBlanca > 3)
        //{
        //    Instantiate(BolaBlancaInstanciar);
        //    contInstanciarBlanca = 0;
        //    gol = false;
        //}

        //Debug.Log(VictoryCondition.entra);
        //Debug.Log(VictoryCondition.desactivarParticulas);

        if (VictoryCondition.entra)
        {
            VictoryCondition.desactivarParticulas += Time.deltaTime;
        }

        if (VictoryCondition.desactivarParticulas > 1.5)
        {
            Particulas1.SetActive(false);
            Particulas2.SetActive(false);
            Particulas3.SetActive(false);
            Particulas4.SetActive(false);
            Particulas5.SetActive(false);
            Particulas6.SetActive(false);
            VictoryCondition.desactivarParticulas = 0;
            VictoryCondition.entra = false;
        }
    }


    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.tag == "Bola" /*|| other.gameObject.tag == "Ball"*/)
    //    {
    //        Debug.Log("+1");
    //        contBolas++;
    //        Destroy(other.gameObject);
    //        //Paredes.SetActive(false);
    //    }
    //}


    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Ball")
        {
            if (this.gameObject.tag == "Arco 1")
            {
                Debug.Log("Gol Violeta");
                GolesVioleta++;
                Debug.Log("Violeta goles: " + GolesVioleta);
                //Particulas1.SetActive(true);
            }
            else
            {
                Debug.Log("Gol Amarillo");
                GolesAmarillo++;
                Debug.Log("Amarillo goles: " + GolesAmarillo);
                //Particulas2.SetActive(true);
            }

            
            
            rbBolaBlanca.velocity *= 0.01f;
            //rbBolaBlanca.velocity *= 0.1f;
            collision.gameObject.transform.position = new Vector3(0.04794955f, 5, 5.953419f);

            VictoryCondition.entra = true;

        }


        if (collision.gameObject.tag == "Bola" )
        {
            //gol = true;





            Debug.Log("+1");
            if (collision.gameObject.name == "Yellow")
            {
                VictoryCondition.contBolasAmarillas++;
            }
            else if (collision.gameObject.name == "Purple")
            {
                VictoryCondition.contBolasVioletas++;
            }
            else if (collision.gameObject.name == "Black")
            {
                VictoryCondition.EntroNegra = true;
            }

            if (this.gameObject.name == "Hoyo1")
            {
                Particulas1.SetActive(true);
            }
            else if (this.gameObject.name == "Hoyo2")
            {
                Particulas2.SetActive(true);
            }
            else if (this.gameObject.name == "Hoyo3")
            {
                Particulas3.SetActive(true);
            }
            else if (this.gameObject.name == "Hoyo4")
            {
                Particulas4.SetActive(true);
            }
            else if (this.gameObject.name == "Hoyo5")
            {
                Particulas5.SetActive(true);
            }
            else if (this.gameObject.name == "Hoyo6")
            {
                Particulas6.SetActive(true);
            }

            VictoryCondition.entra = true;

            Destroy(collision.gameObject);


          
           
        }
    }



}
