using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sombrerito3 : MonoBehaviour
{
    public float contChargeForceVertical;

    public float contChargeForceHorizontal;

    public Rigidbody RbBall;



    public Rigidbody player2;


    private void FixedUpdate()
    {
        Chutar();
        LlevarseLaPelota();
        LevantarPelota();
        PararPelota();
    }


    void Update()
    {


        //if (Player3.tocandoBolaBlanca)
        //{

        //    if (Input.GetKeyDown(KeyCode.Joystick4Button1) && SceneManager.GetSceneByName("FutPool") != SceneManager.GetActiveScene()) //le pego
        //    {
        //        RbBall.AddForce(Player3.movimiento * 5, ForceMode.Impulse);
        //        Debug.Log("Pateando pelota");

        //    }

        //    if (Input.GetKeyDown(KeyCode.Joystick4Button2) && SceneManager.GetSceneByName("FutPool") != SceneManager.GetActiveScene()) //Me la llevo
        //    {

        //        RbBall.isKinematic = true;
        //        RbBall.isKinematic = false;
        //        RbBall.AddForce(Player3.movimiento * 80f, ForceMode.Force);
        //        TutorialController.contLlevarseLaBola++;
        //        //TutorialController.TutoMoverCompleto = true;

        //    }

        //    if (Input.GetKey(KeyCode.Joystick4Button5) && SceneManager.GetSceneByName("FutPool") != SceneManager.GetActiveScene()) //la paro
        //    {

        //        RbBall.transform.LookAt(player2.transform);
        //        RbBall.AddForce(RbBall.transform.forward, ForceMode.Acceleration);
        //        RbBall.velocity *= 0.1f;

        //    }

        //    if (Input.GetKeyUp(KeyCode.Joystick4Button3)) //sombrerito/centro
        //    {
        //        if (contChargeForceVertical > 20)
        //        {
        //            contChargeForceVertical = 20;
        //        }
        //        if (contChargeForceHorizontal > 5)
        //        {
        //            contChargeForceHorizontal = 5;
        //        }

        //        RbBall.AddForce(player2.transform.up * 10, ForceMode.VelocityChange);
        //        RbBall.AddForce(Player3.movimiento * contChargeForceHorizontal, ForceMode.Impulse);

        //        contChargeForceVertical = 0;
        //        contChargeForceHorizontal = 0;
        //    }
        //}

        //if (Input.GetKey(KeyCode.Joystick4Button3))
        //{
        //    contChargeForceVertical += Time.deltaTime * 50;
        //    contChargeForceHorizontal += Time.deltaTime * 20;


        //}

        //if (Input.GetKeyUp(KeyCode.Joystick4Button3))
        //{
        //    contChargeForceVertical = 0;
        //    contChargeForceHorizontal = 0;
        //}
    }



    public void Chutar(/*InputAction.CallbackContext callbackContext*/)
    {
        if (Player3.tocandoBolaBlanca)
        {
            if (JoystickPlayerSelection.gamepad3.buttonEast.wasPressedThisFrame && SceneManager.GetSceneByName("FutPool") != SceneManager.GetActiveScene()) //le pego
            {

                //if (callbackContext2.performed)
                //{
                //    RbBall.AddForce(PlayerMovement.movimiento * 5, ForceMode.Impulse);
                //}



                RbBall.AddForce(Player3.movimiento * 5 /*/ 6*/, ForceMode.Impulse);
                //Debug.Log("ENTRA PEGELE A LA PELOTA");
            }
        }
    }

    public void LlevarseLaPelota()
    {
        if (Player3.tocandoBolaBlanca)
        {


            if (JoystickPlayerSelection.gamepad3.buttonWest.wasPressedThisFrame && SceneManager.GetSceneByName("FutPool") != SceneManager.GetActiveScene()) //Me la llevo
            {

                RbBall.isKinematic = true;
                RbBall.isKinematic = false;
                RbBall.AddForce(Player3.movimiento * 80f, ForceMode.Force);
                TutorialController.contLlevarseLaBola++;


            }
        }
    }


    public void LevantarPelota()
    {
        if (Player3.tocandoBolaBlanca)
        {
            if (JoystickPlayerSelection.gamepad3.buttonNorth.wasReleasedThisFrame) //sombrerito/centro
            {

                if (contChargeForceVertical > 20)
                {
                    contChargeForceVertical = 20;
                }
                if (contChargeForceHorizontal > 5)
                {
                    contChargeForceHorizontal = 5;
                }

                RbBall.AddForce(player2.transform.up * 10, ForceMode.VelocityChange);
                RbBall.AddForce(Player3.movimiento * contChargeForceHorizontal, ForceMode.Impulse);

                contChargeForceVertical = 0;
                contChargeForceHorizontal = 0;
            }
        }

        if (JoystickPlayerSelection.gamepad3.buttonNorth.isPressed)
        {

            contChargeForceVertical += Time.deltaTime * 50;
            contChargeForceHorizontal += Time.deltaTime * 20;


        }

        if (JoystickPlayerSelection.gamepad3.buttonNorth.wasReleasedThisFrame)
        {
            contChargeForceVertical = 0;
            contChargeForceHorizontal = 0;
        }

    }

    public void PararPelota()
    {
        if (Player3.tocandoBolaBlanca)
        {
            if (JoystickPlayerSelection.gamepad3.rightShoulder.wasReleasedThisFrame && SceneManager.GetSceneByName("FutPool") != SceneManager.GetActiveScene()) //la paro
            {

                RbBall.transform.LookAt(player2.transform);
                RbBall.AddForce(RbBall.transform.forward, ForceMode.Acceleration);
                RbBall.velocity *= 0.1f;

            }
        }
    }


}
