using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using TMPro;

public class NumeroDeDashs : MonoBehaviour
{
    public Transform follow;

    public TMP_Text text1;
    public TMP_Text text2;
    public TMP_Text text3;
    public TMP_Text text4;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.name == "P2CantidadDash")
        {
            text2.text = DashingP2.DashesDisponibles.ToString();
        }
        else if (this.gameObject.name == "P1CantidadDash")
        {
            text1.text = Dashing.DashesDisponibles.ToString();
        }
        else if (this.gameObject.name == "P3CantidadDash")
        {
            text3.text = DashingP3.DashesDisponibles.ToString();
        }
        else if (this.gameObject.name == "P4CantidadDash")
        {
            text4.text = DashingP4.DashesDisponibles.ToString();
        }
        transform.position = follow.position + new Vector3(1.5f,0.5f,2.7f);
    }
}
