using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuController : MonoBehaviour
{
    public GameObject Bola1;
    public GameObject Bola2;
    public GameObject Bola3;
    public GameObject Bola4;

    public GameObject Pj;

    public Button b1;
    public Button b2;
    public Button b3;
    public Button b4;



    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (b1 == EventSystem.current.currentSelectedGameObject.GetComponent<Button>())
        {
            B1();  
        }
        else if (b2 == EventSystem.current.currentSelectedGameObject.GetComponent<Button>())
        {
            B2();
        }
        else if (b3 == EventSystem.current.currentSelectedGameObject.GetComponent<Button>())
        {
            B3();
        }
        else if (b4 == EventSystem.current.currentSelectedGameObject.GetComponent<Button>())
        {
            B4();
        }
        else
        {

        }




        Pj.transform.Rotate(new Vector3(0, 100, 0) * Time.deltaTime);
    }

    public void STF()
    {
        SceneManager.LoadScene(1);
    }

    public void FP()
    {
        SceneManager.LoadScene(2);
    }

    public void T()
    {
        SceneManager.LoadScene(3);
    }

    public void FreeKick()
    {
        //Application.Quit();
        SceneManager.LoadScene(4);
    }

    public void PJVSIA()
    {
        //Application.Quit();
        SceneManager.LoadScene(5);
    }



    public void B1()
    {
        Bola1.SetActive(true);
        Bola2.SetActive(false);
        Bola3.SetActive(false);
        Bola4.SetActive(false);
    }

    public void B2()
    {
        Bola1.SetActive(false);
        Bola2.SetActive(true);
        Bola3.SetActive(false);
        Bola4.SetActive(false);
    }


    public void B3()
    {
        Bola1.SetActive(false);
        Bola2.SetActive(false);
        Bola3.SetActive(true);
        Bola4.SetActive(false);
    }

    public void B4()
    {
        Bola1.SetActive(false);
        Bola2.SetActive(false);
        Bola3.SetActive(false);
        Bola4.SetActive(true);
    }



}
