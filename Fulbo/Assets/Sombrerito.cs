using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class Sombrerito : MonoBehaviour
{
    public float contChargeForceVertical;

    public float contChargeForceHorizontal;

    public Rigidbody RbBall;

    //public InputActionAsset asset;

    public Rigidbody player2;

    //public PlayerControls playerControls;


    void Start()
    {

    }

    private void FixedUpdate()
    {
        if (Gamepad.all.Count == 1 || Gamepad.all.Count > 1)
        {


            Chutar();
            LlevarseLaPelota();
            LevantarPelota();
            PararPelota();
        }
    }


    void Update()
    {

        //Debug.Log(callbackContext);
        //if (PlayerMovement.tocandoBolaBlanca)
        //{


        //    //if (JoystickPlayerSelection.gamepad1.buttonEast.wasPressedThisFrame && SceneManager.GetSceneByName("FutPool") != SceneManager.GetActiveScene()) //le pego
        //    //{

        //    //    RbBall.AddForce(PlayerMovement.movimiento * 5, ForceMode.Impulse);

        //    //}


        //    if (/*Input.GetKeyDown(KeyCode.Joystick1Button2)*/ JoystickPlayerSelection.gamepad1.buttonWest.wasPressedThisFrame && SceneManager.GetSceneByName("FutPool") != SceneManager.GetActiveScene()) //Me la llevo
        //    {

        //        RbBall.isKinematic = true;
        //        RbBall.isKinematic = false;
        //        RbBall.AddForce(PlayerMovement.movimiento * 80f, ForceMode.Force);
        //        TutorialController.contLlevarseLaBola++;
        //        //TutorialController.TutoMoverCompleto = true;

        //    }

        //    if (Input.GetKey(KeyCode.Joystick1Button5) && SceneManager.GetSceneByName("FutPool") != SceneManager.GetActiveScene()) //la paro
        //    {

        //        RbBall.transform.LookAt(player2.transform);
        //        RbBall.AddForce(RbBall.transform.forward, ForceMode.Acceleration);
        //        RbBall.velocity *= 0.1f;

        //    }

        //    if (Input.GetKeyUp(KeyCode.Joystick1Button3)) //sombrerito/centro
        //    {
        //        if (contChargeForceVertical > 20)
        //        {
        //            contChargeForceVertical = 20;
        //        }
        //        if (contChargeForceHorizontal > 5)
        //        {
        //            contChargeForceHorizontal = 5;
        //        }

        //        RbBall.AddForce(player2.transform.up * 10, ForceMode.VelocityChange);
        //        RbBall.AddForce(PlayerMovement.movimiento * contChargeForceHorizontal, ForceMode.Impulse);

        //        contChargeForceVertical = 0;
        //        contChargeForceHorizontal = 0;
        //    }
        //}

        //if (Input.GetKey(KeyCode.Joystick1Button3))
        //{
        //    contChargeForceVertical += Time.deltaTime * 50;
        //    contChargeForceHorizontal += Time.deltaTime * 20;


        //}

        //if (Input.GetKeyUp(KeyCode.Joystick1Button3))
        //{
        //    contChargeForceVertical = 0;
        //    contChargeForceHorizontal = 0;
        //}



        //Chutar(/*callbackContext2*/);
        //LlevarseLaPelota();
        //LevantarPelota();
        //PararPelota();
      
    }



    public void Chutar(/*InputAction.CallbackContext callbackContext*/)
    {
        if (PlayerMovement.tocandoBolaBlanca)
        {
            if (JoystickPlayerSelection.gamepad1.buttonEast.wasPressedThisFrame && SceneManager.GetSceneByName("FutPool") != SceneManager.GetActiveScene()) //le pego
            {
                
                //if (callbackContext2.performed)
                //{
                //    RbBall.AddForce(PlayerMovement.movimiento * 5, ForceMode.Impulse);
                //}
             


                RbBall.AddForce(PlayerMovement.movimiento * 5 /*/ 6*/, ForceMode.Impulse);
                //Debug.Log(callbackContext.disabled);
            }
        }
    }

    public void LlevarseLaPelota()
    {
        if (PlayerMovement.tocandoBolaBlanca)
        {


            if (JoystickPlayerSelection.gamepad1.buttonWest.wasPressedThisFrame && SceneManager.GetSceneByName("FutPool") != SceneManager.GetActiveScene()) //Me la llevo
            {

                RbBall.isKinematic = true;
                RbBall.isKinematic = false;
                RbBall.AddForce(PlayerMovement.movimiento * 80f, ForceMode.Force);
                TutorialController.contLlevarseLaBola++;


            }
        }
    }


    public void LevantarPelota()
    {
        if (PlayerMovement.tocandoBolaBlanca)
        {
            if (JoystickPlayerSelection.gamepad1.buttonNorth.wasReleasedThisFrame) //sombrerito/centro
            {

                if (contChargeForceVertical > 20)
                {
                    contChargeForceVertical = 20 ;
                }
                if (contChargeForceHorizontal > 5)
                {
                    contChargeForceHorizontal = 5;
                }
          
                RbBall.AddForce(player2.transform.up * 10 , ForceMode.VelocityChange);
                RbBall.AddForce(PlayerMovement.movimiento * contChargeForceHorizontal , ForceMode.Impulse);
            
                contChargeForceVertical = 0;
                contChargeForceHorizontal = 0;
            }
        }

        if (JoystickPlayerSelection.gamepad1.buttonNorth.isPressed)
        {
         
            contChargeForceVertical += Time.deltaTime * 50;
            contChargeForceHorizontal += Time.deltaTime * 20;


        }

        if (JoystickPlayerSelection.gamepad1.buttonNorth.wasReleasedThisFrame)
        {
            contChargeForceVertical = 0;
            contChargeForceHorizontal = 0;
        }

    }

    public void PararPelota()
    {
        if (PlayerMovement.tocandoBolaBlanca)
        {
            if (JoystickPlayerSelection.gamepad1.rightShoulder.wasReleasedThisFrame && SceneManager.GetSceneByName("FutPool") != SceneManager.GetActiveScene()) //la paro
            {

                RbBall.transform.LookAt(player2.transform);
                RbBall.AddForce(RbBall.transform.forward, ForceMode.Acceleration);
                RbBall.velocity *= 0.1f;

            }
        }
    }

}
