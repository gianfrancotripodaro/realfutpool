using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SaltoPlayer : MonoBehaviour
{

    public Rigidbody rb;

    public float JumpForce;

    public bool onFloor;

    public GameObject polvoCaida;
    //public GameObject polvoSalto;

    public static float timerPolvoCaida;

    public bool saltoycayo;


    public bool Salto1;

    public bool Salto2;

    public Vector3 pPosition;



    public float tiempoaire;




    private void FixedUpdate()
    {

        if (JoystickPlayerSelection.gamepad1.buttonSouth.wasPressedThisFrame)
        {


            Salto1 = true;

        }

        pPosition = this.transform.position + new Vector3(0,-0.2f,0);

        if (Salto1)
        {
            //StartCoroutine(Saltar());
            
            rb.AddForce(rb.transform.up * 10, ForceMode.Impulse);
        }

        if (!onFloor)
        {
            //StartCoroutine(Caer());
            tiempoaire += Time.deltaTime;

            if (tiempoaire > 0.2)
            {
                Salto1 = false;
                Salto2 = true;
            }

            rb.AddForce(transform.up * -4f /** Time.deltaTime*/, ForceMode.Impulse);

        }
        else
        {
            tiempoaire = 0;
            //onFloor = true;
        }


    }


    private void Update(/*InputAction.CallbackContext callbackContext*/)
    {

      

        if (saltoycayo)
        {
            if (polvoCaida)
            {
                polvoCaida.SetActive(true);
            }
            else
            {
                Debug.Log("Borrado");
            }
            

            //Instantiate(polvoCaida, pPosition, rb.rotation);

            timerPolvoCaida += Time.deltaTime;
            if (timerPolvoCaida > 0.8)
            {
               
                //polvoCaida.SetActive(false);
                timerPolvoCaida = 0;
                saltoycayo = false;
            }
        }


        //if (onFloor && Input.GetKeyDown(KeyCode.Joystick1Button0))
        //{
          
        //    Salto1 = true;
        //}



    }


   public IEnumerator Saltar()
    {
        Salto1 = false;
        float startTime = Time.time;
        while (Time.time < startTime + 0.30f)
        {
            Salto2 = true;
            rb.AddForce(transform.up * JumpForce /** Time.deltaTime*/, ForceMode.Impulse);
            yield return null;
        }
        //Salto1 = false;

    //rb.AddForce(-transform.up * 10, ForceMode.VelocityChange);
    }

    IEnumerator Caer()
    {
        float startTime = Time.time;
       
        while (Time.time < startTime + 0.01f /*|| onFloor*/)
        {

            rb.AddForce(transform.up * -1.2f /** Time.deltaTime*/, ForceMode.Impulse);
            yield return null;
        }
        
        //if (onFloor)
        //{
        //    saltoycayo = true;
        //}

    }

    //public void caer2()
    //{
    //    rb.AddForce(transform.up * -1.2f /** Time.deltaTime*/, ForceMode.Impulse);
    //}


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            onFloor = true;

            if (Salto2)
            {
                saltoycayo = true;
                Salto2 = false;
                Instantiate(polvoCaida, pPosition, rb.rotation);
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            onFloor = false;
        }
    }

    //public void Saltar1(InputAction.CallbackContext callbackContext)
    //{

    //    Salto1 = true;

    //    Debug.Log("salta");
    //    Debug.Log(callbackContext.phase);
    //}

}
