using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaltoPlayer4 : MonoBehaviour
{
    public Rigidbody rb;

    public float JumpForce;



    public bool onFloor;


    public GameObject polvoCaida;

    public static float timerPolvoCaida;

    public bool saltoycayo;

    public Vector3 pPosition;

    public bool Salto1;

    public bool Salto2;

    public float tiempoaire;

    private void FixedUpdate()
    {

        if (JoystickPlayerSelection.gamepad4.buttonSouth.wasPressedThisFrame)
        {
            //polvoSalto.SetActive(true);
            //StartCoroutine(Saltar());
            Salto1 = true;

        }

        pPosition = this.transform.position + new Vector3(0, -0.2f, 0);

        if (Salto1)
        {
            //StartCoroutine(Saltar());

            rb.AddForce(rb.transform.up * 10, ForceMode.Impulse);
        }

        if (!onFloor)
        {
            //StartCoroutine(Caer());
            tiempoaire += Time.deltaTime;

            if (tiempoaire > 0.2)
            {
                Salto1 = false;
                Salto2 = true;
            }

            rb.AddForce(transform.up * -4f /** Time.deltaTime*/, ForceMode.Impulse);

        }
        else
        {
            tiempoaire = 0;
        }


    }


    private void Update()
    {

        if (saltoycayo)
        {
            if (polvoCaida)
            {
                polvoCaida.SetActive(true);
            }
            else
            {
                Debug.Log("Borrado");
            }


            //Instantiate(polvoCaida, pPosition, rb.rotation);

            timerPolvoCaida += Time.deltaTime;
            if (timerPolvoCaida > 0.8)
            {

                //polvoCaida.SetActive(false);
                timerPolvoCaida = 0;
                saltoycayo = false;
            }
        }


        if (JoystickPlayerSelection.gamepad4.buttonSouth.wasPressedThisFrame)
        {
            //polvoSalto.SetActive(true);
            //StartCoroutine(Saltar());
            Salto1 = true;

        }
        //else
        //{
        //    StartCoroutine(Caer());
        //}
    }


    //IEnumerator Saltar()
    //{
    //    float startTime = Time.time;
    //    while (Time.time < startTime + 0.30f)
    //    {

    //        rb.AddForce(transform.up * JumpForce, ForceMode.Impulse);
    //        yield return null;
    //    }

    //    //rb.AddForce(-transform.up * 10, ForceMode.VelocityChange);
    //}

    //IEnumerator Caer()
    //{
    //    float startTime = Time.time;
    //    while (Time.time < startTime + 0.01f /*|| onFloor*/)
    //    {

    //        rb.AddForce(transform.up * -0.5f, ForceMode.Impulse);
    //        yield return null;
    //    }
    //}


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            onFloor = true;

            if (Salto2)
            {
                saltoycayo = true;
                Salto2 = false;
                Instantiate(polvoCaida, pPosition, rb.rotation);
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            onFloor = false;
        }
    }
}
