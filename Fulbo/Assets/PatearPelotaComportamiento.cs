using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatearPelotaComportamiento : StateMachineBehaviour
{
    public StatesMachineController accionesIA;


    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("PATEA");

        //accionesIA.ChutarIA();
        accionesIA.PatearPelota();

    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        accionesIA = animator.gameObject.GetComponent<StatesMachineController>();
    }

}
