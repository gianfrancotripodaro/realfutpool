using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pausa : MonoBehaviour
{
    public bool pausaOn;

    public GameObject PausaPanel;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (pausaOn && Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            SceneManager.LoadScene(0);
            Time.timeScale = 1;
        }

        if (pausaOn && Input.GetKeyDown(KeyCode.Joystick2Button1))
        {
            SceneManager.LoadScene(0);
            Time.timeScale = 1;
        }


        if (Input.GetKeyDown(KeyCode.Joystick1Button7))
        {

            if (pausaOn)
            {
                //JuegoNOPausado
                Debug.Log("Start");
                Time.timeScale = 1;
                pausaOn = false;
                PausaPanel.SetActive(false);
            }
            else
            {
                //JuegoPausado
                Debug.Log("Start");
                Time.timeScale = 0;
                pausaOn = true;
                PausaPanel.SetActive(true);

            }
        }

        if (Input.GetKeyDown(KeyCode.Joystick2Button7) && !pausaOn)
        {
            if (pausaOn)
            {
                Debug.Log("Start");
                Time.timeScale = 1;
                pausaOn = false;
            }
            else
            {
                Debug.Log("Start");
                Time.timeScale = 0;
                pausaOn = true;
            }
        }
    
      
    }
}
