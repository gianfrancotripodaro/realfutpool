using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player3 : MonoBehaviour
{
    public float velocidad = 5f;

    private Rigidbody rb;

    //public float rayDistance;

    //public GameObject apuntar;

    public static bool tocandoBolaBlanca;

    public static bool Catch;

    //public int jugadorID;


    public GameObject ball;

    public float ballDistance;

    public float ballForce;

    public static bool holdingball;

    Rigidbody ballRB;

    public bool throwingBall;

    //public float timerRotation;

    //public bool RotoTodo;

    public bool ajustoFuerza;


    public TrajectoryController tC;

    //public GameObject normalCamara;
    //public GameObject shootCamara;


    public static Rigidbody rbBolaBlanca_;

    public static Vector3 movimiento;

    public static bool tocandoBolaColor;

    public float timerLowSpeed;

    public GameObject Espada;
    public GameObject Cansado;
    //public GameObject Escudo;
    public GameObject Mareado;
    public GameObject Mareado2;

    public GameObject pataPateando;


    public Animator animator;
    public string variableMovimiento;
    public GameObject Personaje;

    public float TurnSmoothTime = 0.1f;
    public float TurnSmoothVelocity = 0.1f;

    private void Awake()
    {
        ballRB = ball.GetComponent<Rigidbody>();
        rb = GetComponent<Rigidbody>();

        tC = tC.GetComponent<TrajectoryController>();
        //tC =

        Physics.IgnoreLayerCollision(6, 7, false);
    }

    public void LateUpdate()
    {
        if (holdingball)
        {
            //ball.transform.position = apuntar.transform.position + this.transform.forward * ballDistance;

            //if (Input.GetKey(KeyCode.Joystick2Button3))
            //{

            //    normalCamara.SetActive(true);
            //    shootCamara.SetActive(false);
            //}
            //else
            //{
            //    normalCamara.SetActive(false);
            //    shootCamara.SetActive(true);
            //}
        }

        //if (throwingBall)
        //{
        //    ballRB.AddForce(rb.transform.forward * ballForce/*, ForceMode.VelocityChange*/);
        //    throwingBall = false;
        //}
    }

    public void Update()
    {



        if (DashingP3.contStun == 0)
        {
            Mareado.SetActive(false);
            Mareado2.SetActive(false);
            //animator.SetBool("Stuneado", false);
        }

        if (DashingP3.barriendo)
        {
            Espada.SetActive(true);
        }
        else
        {
            Espada.SetActive(false);
        }

        if ((Dashing.P1StuneoAPJ || DashingP2.PJ2StuneoAPJ) && (DashingP2.P2AP3 || Dashing.P1AP3) || (/*DashingP4.PJ4StuneoAPJ ||*/ DashingP4.P4AP3))
        {
            if (!DashingP3.hiceParry)
            {
                Mareado.SetActive(true);
                animator.SetBool("Stuneado", true);
                Debug.Log("Entra????");
            }
            else
            {
                Mareado.SetActive(false);
                //animator.SetBool("Stuneado", false);
            }

        }
        else
        {
            Mareado.SetActive(false);
            animator.SetBool("Stuneado", false);
        }

        //if (DashingP4.PJ4StuneoAPJ)
        //{
        //    if (!DashingP3.hiceParry)
        //    {
        //        Mareado.SetActive(true);
        //        animator.SetBool("Stuneado", true);
        //    }
        //    else
        //    {
        //        Mareado.SetActive(false);
        //        //animator.SetBool("Stuneado", false);
        //    }

        //}
        //else
        //{
        //    Mareado.SetActive(false);
        //    animator.SetBool("Stuneado", false);
        //}

        if (DashingP3.Escudo2)
        {
            Mareado.SetActive(false);
            Mareado2.SetActive(false);
            //animator.SetBool("Stuneado", false);
        }
        else
        {
            if (Dashing.P1ParryAP3 || DashingP2.P2ParryAP3 || DashingP4.P4ParryAP3)
            {
                Mareado2.SetActive(true);
                animator.SetBool("Stuneado", true);
            }
            else
            {
                //Escudo.SetActive(false);
                Mareado2.SetActive(false);
                //animator.SetBool("Stuneado", false);

            }

            //if (DashingP2.hiceParry)
            //{
            //    Mareado2.SetActive(true);
            //    animator.SetBool("Stuneado", true);
            //}
            //else
            //{
            //    //Escudo.SetActive(false);
            //    Mareado2.SetActive(false);
            //    //animator.SetBool("Stuneado", false);

            //}

            //if (DashingP4.hiceParry)
            //{
            //    Mareado2.SetActive(true);
            //    animator.SetBool("Stuneado", true);
            //}
            //else
            //{
            //    //Escudo.SetActive(false);
            //    Mareado2.SetActive(false);
            //    //animator.SetBool("Stuneado", false);

            //}
        }



        if (DashingP3.contStun == 0)
        {
            Debug.Log("SEACABA EL STUN");
            Mareado.SetActive(false);
            Mareado2.SetActive(false);
            animator.SetBool("Stuneado", false);
        }

        if (TrajectoryController.disparoLaPelotaJ2 && SceneManager.GetSceneByName("Futpool") == SceneManager.GetActiveScene())
        {
            timerLowSpeed += Time.deltaTime;
            velocidad = 1f;
            Cansado.SetActive(true);
            if (timerLowSpeed > 2)
            {
                Cansado.SetActive(false);
                velocidad = 5f;
                timerLowSpeed = 0;
                TrajectoryController.disparoLaPelotaJ2 = false;
            }
        }

        if (VictoryCondition.PateaTeam == 2)
        {
            DetectarBola();
        }

        //DetectarMirando();
    }

    private void FixedUpdate()
    {

        CatchBall();




        // Verificar el jugador actual y obtener las entradas de acuerdo a su ID
        float movimientoHorizontal = 0f;
        float movimientoVertical = 0f;


        //movimientoHorizontal = Input.GetAxis("verticalMando3");
        //movimientoVertical = Input.GetAxis("horizontalMando3");

        movimientoHorizontal = JoystickPlayerSelection.gamepad3.leftStick.ReadValue().y;
        movimientoVertical = JoystickPlayerSelection.gamepad3.leftStick.ReadValue().x;


        animator.SetFloat(variableMovimiento, (Mathf.Abs(movimientoVertical) + Mathf.Abs(movimientoHorizontal)));


        //float targetAngle = Mathf.Atan2(movimiento.x, movimiento.z) * Mathf.Rad2Deg;

        //float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref TurnSmoothVelocity, TurnSmoothTime);

        //transform.rotation = Quaternion.Euler(0f, angle, 0f);

        if (movimiento != Vector3.zero)
        {
            Quaternion targetRotation = Quaternion.LookRotation(movimiento);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 7 * Time.deltaTime);

        }
        else
        {
            // Rotaci�n de 180 grados en el eje Y
            Quaternion targetRotation = Quaternion.Euler(0, 180, 0);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 7 * Time.deltaTime);

        }

        if (/*Input.GetAxis("RTJ3") > 0.2F*/ JoystickPlayerSelection.gamepad3.rightTrigger.isPressed)
        {
            movimiento = new Vector3(-movimientoHorizontal, 0f, movimientoVertical) * velocidad * 2;
        }
        else
        {
            movimiento = new Vector3(-movimientoHorizontal, 0f, movimientoVertical) * velocidad;
        }



        rb.velocity = movimiento;
    }


    public void DetectarMirando()
    {
        //Debug.DrawRay(apuntar.transform.position, apuntar.transform.TransformDirection(new Vector3(1, 0, 0)) * rayDistance, Color.red);
        //RaycastHit hit;


        //if (Physics.Raycast(apuntar.transform.position, apuntar.transform.TransformDirection(new Vector3(1, 0, 0)), out hit, rayDistance))
        //{
        //    //ACA ESCRIBO LOS COMPORTAMIENTOS QUE TENGA SEGUN CON CADA OBJETO
        //    Debug.Log(hit.transform.tag);

        //    //if (!ChatController.guardado && hit.transform.tag == "Player")
        //    //{
        //    //    Debug.Log("Perdiste");
        //    //    Perdiste = true;

        //    //}

        //    //if (!playerController.vistaAlfrente && hit.transform.tag == "Player")
        //    //{
        //    //    Debug.Log("Perdiste");
        //    //    Perdiste = true;
        //    //}

        //}
        //else
        //{
        //    Debug.Log("tranca");
        //}

    }




    public void DetectarBola()
    {

        //if (Input.GetKeyDown(KeyCode.Joystick2Button0) && DashingP2.contStun >2)
        //{
        //    rb.isKinematic = false;
        //}


        if (Input.GetKeyDown(KeyCode.Joystick2Button0) && tocandoBolaBlanca)
        {
            ajustoFuerza = false;
            Debug.Log("Catch");

            if (SceneManager.GetSceneByName("Futpool") == SceneManager.GetActiveScene())
            {
                tC.UpdateTrajectory();
            }


            //Comienza a dar vueltas
            if (Catch)
            {
                holdingball = false;
                Catch = false;
                throwingBall = true;

                //Debug.Log("Hola");


                //ballRB = Instantiate(ballRB, apuntar.transform.position, Quaternion.identity);

                //if (throwingBall)
                //{




                //ballRB.AddForce(rb.transform.forward * ballForce, ForceMode.Impulse);
                //if (ajustoFuerza)
                //{
                //normalCamara.SetActive(true);
                //shootCamara.SetActive(false);
                if (SceneManager.GetSceneByName("Futpool") == SceneManager.GetActiveScene())
                {
                    tC.Disparar();
                }

                rb.isKinematic = false;
                VictoryCondition.ContTimesPatadasVioloeta--;
                TrajectoryController.disparoLaPelotaJ2 = true;


                //Cansado.SetActive(true);
                //llama a otro script para disparar o no
                //}
                //else
                //{
                //    //frenar el giro de la trayectoria y aparece el metodo de ajustar la fuerza.
                //   //TrajectoryController.forceAngle = 0;
                //}





                //throwingBall = false;
                //}

            }
            else
            {

                //Cansado.SetActive(false);
                holdingball = true;

                Catch = true;
            }



        }
    }

    public void CatchBall()
    {
        if (Catch && SceneManager.GetSceneByName("Futpool") == SceneManager.GetActiveScene())
        {

            pataPateando.SetActive(true);

            if (tocandoBolaBlanca)
            {
                rb.isKinematic = true;
            }
            else
            {
                rb.isKinematic = false;
                Catch = false;
            }


            ballRB.velocity *= 0.1f;

            //timerRotation += Time.deltaTime;

            //transform.Rotate(new Vector3(0f, 100f, 0f) * Time.deltaTime);

            //if (transform.rotation.eulerAngles.y < 150 && RotoTodo == false)
            //{
            //    transform.Rotate(new Vector3(0f, 100f, 0f) * Time.deltaTime);
            //}
            //else
            //{
            //    RotoTodo = true;
            //    transform.Rotate(new Vector3(0f, -100f, 0f) * Time.deltaTime);

            //    if (transform.rotation.eulerAngles.y < 50)
            //    {
            //        RotoTodo = false;
            //    }

            //}




        }
        else
        {
            pataPateando.SetActive(false);

            if (Dashing.P1StuneoAPJ)
            {
                //rb.isKinematic = true;
                //if (Dashing.barriendo)
                //{
                //    rb.isKinematic = true;
                //}
                //Mareado.SetActive(true);
                Debug.Log("ESTOY STUNEADO");
            }
            else
            {


                //Mareado.SetActive(false);
                //Debug.Log("Soy libre");
                //rb.isKinematic = false;
                //if (!Dashing.Escudo)
                //{
                //    rb.isKinematic = false;
                //}


            }
            //transform.Rotate(new Vector3(0f, 0, 0f));
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ball")
        {
            Debug.Log("TOCANDO LA BOLA BLANCAAAA");
            tocandoBolaBlanca = true;
            rbBolaBlanca_ = other.gameObject.GetComponent<Rigidbody>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Ball")
        {
            tocandoBolaBlanca = false;
            rb.isKinematic = false;
        }
    }
}
