using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;



public class PlayerMovement : MonoBehaviour
{


  

    public float velocidad = 5f;

    private Rigidbody rb;

    //public float rayDistance;

    //public GameObject apuntar;

    public static bool tocandoBolaBlanca;

    public static bool Catch;

    public int jugadorID;


    public GameObject ball;

    public float ballDistance;

    public float ballForce;

    public static bool holdingball;

    Rigidbody ballRB;

    public bool throwingBall;

    public float timerRotation;

    public bool RotoTodo;

    public bool ajustoFuerza;


    //public TrajectoryController tC;

    //public GameObject normalCamara;
    //public GameObject shootCamara;

    public static Rigidbody rbBolaBlanca_2;


    public static Vector3 movimiento;

    public static bool tocandoBolaColor;

    public float timerLowSpeed;

    public GameObject Espada;
    public GameObject Cansado;
    //public GameObject Escudo;
    public GameObject Mareado;
    public GameObject Mareado2;

    //public static bool Parry;

    public GameObject pataPateando;


    public Animator animator;
    public string variableMovimiento;
    public GameObject Personaje;

    public float TurnSmoothTime= 0.1f;
    public float TurnSmoothVelocity = 0.1f;


    //[Space(10)]
    //[Tooltip("The height the player can jump")]
    //public float JumpHeight = 1.2f;
    //[Tooltip("The character uses its own gravity value. The engine default is -9.81f")]
    //public float Gravity = -15.0f;

    //[Space(10)]
    //[Tooltip("Time required to pass before being able to jump again. Set to 0f to instantly jump again")]
    //public float JumpTimeout = 0.1f;
    //[Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
    //public float FallTimeout = 0.15f;


    //[Header("Player Grounded")]
    //[Tooltip("If the character is grounded or not. Not part of the CharacterController built in grounded check")]
    //public bool Grounded = true;
    //[Tooltip("Useful for rough ground")]
    //public float GroundedOffset = -0.14f;
    //[Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
    //public float GroundedRadius = 0.5f;
    //[Tooltip("What layers the character uses as ground")]
    //public LayerMask GroundLayers;


    //// timeout deltatime
    //private float _jumpTimeoutDelta;
    //private float _fallTimeoutDelta;
    //private StarterAssetsInputs _input;
    //private float _verticalVelocity;


    private void Awake()
    {
        ballRB = ball.GetComponent<Rigidbody>();
        rb = GetComponent<Rigidbody>();

        //tC = tC.GetComponent<TrajectoryController>();
        //tC =


        Physics.IgnoreLayerCollision(6, 7, false);
    }

    public void LateUpdate()
    {
        if (holdingball)
        {
            //ball.transform.position = apuntar.transform.position + this.transform.forward * ballDistance;




            //if (Input.GetKey(KeyCode.Joystick1Button3))
            //{

            //    normalCamara.SetActive(true);
            //    shootCamara.SetActive(false);
            //}
            //else
            //{
            //    normalCamara.SetActive(false);
            //    shootCamara.SetActive(true);
            //}
        }
   

        //if (throwingBall)
        //{
        //    ballRB.AddForce(rb.transform.forward * ballForce/*, ForceMode.VelocityChange*/);
        //    throwingBall = false;
        //}
    }

    public void Update()
    {




        if (Dashing.hiceParry)
        {
            Mareado.SetActive(false);
            Mareado2.SetActive(false);
            animator.SetBool("Stuneado", false);
        }



        if (Dashing.Escudo)
        {
            Mareado.SetActive(false);
            Mareado2.SetActive(false);
            animator.SetBool("Stuneado", false);
        }
        else
        {
            if (DashingP2.P2ParryAP1 || DashingP3.P3ParryAP1 || DashingP4.P4ParryAP1)
            {
                Mareado.SetActive(true);
                animator.SetBool("Stuneado", true);
            }
            else
            {
                Mareado.SetActive(false);
                animator.SetBool("Stuneado", false);
            }
            if ((DashingP2.PJ2StuneoAPJ && DashingP2.P2AP1) || (DashingP3.PJ3StuneoAPJ && DashingP3.P3AP1) || (DashingP4.PJ4StuneoAPJ && DashingP4.P4AP1))
            {
                if (!Dashing.hiceParry)
                {
                    Mareado2.SetActive(true);
                    animator.SetBool("Stuneado", true);
                }

                Debug.Log("Entra");
            }
            else
            {
                //Escudo.SetActive(false);
                Mareado2.SetActive(false);
                animator.SetBool("Stuneado", false);
            }
        }

        if (Dashing.contStun == 0)
        {
            Mareado.SetActive(false);
            //Mareado2.SetActive(false);
            //animator.SetBool("Stuneado", false);
        }





        if (Dashing.barriendo)
        {
            Espada.SetActive(true);
        }
        else
        {
            Espada.SetActive(false);
        }

        if (TrajectoryController.disparoLaPelotaJ1 && SceneManager.GetSceneByName("Futpool") == SceneManager.GetActiveScene())
        {
            timerLowSpeed += Time.deltaTime;
            velocidad = 1f;
            Cansado.SetActive(true);

            if (timerLowSpeed>2)
            {
                Cansado.SetActive(false);
                velocidad = 5f;
                timerLowSpeed = 0;
                TrajectoryController.disparoLaPelotaJ1 = false;
            }
        }

        if (VictoryCondition.PateaTeam == 1)
        {
            DetectarBola();
        }

    }

    private void FixedUpdate()
    {
        

        CatchBall();


        float movimientoHorizontal = 0f;
        float movimientoVertical = 0f;


        //movimientoHorizontal = Input.GetAxis("Vertical");
        //movimientoVertical = Input.GetAxis("Horizontal");


       

            movimientoHorizontal = -JoystickPlayerSelection.gamepad1.leftStick.ReadValue().y;
            movimientoVertical = JoystickPlayerSelection.gamepad1.leftStick.ReadValue().x;

            animator.SetFloat(variableMovimiento, (Mathf.Abs(movimientoVertical) + Mathf.Abs(movimientoHorizontal)));


            float targetAngle = Mathf.Atan2(movimiento.x, movimiento.z) * Mathf.Rad2Deg;

            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref TurnSmoothVelocity, TurnSmoothTime);

            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            if (/*Input.GetAxis("RTJ1") > 0.2F*/ JoystickPlayerSelection.gamepad1.rightTrigger.isPressed)
            {
                movimiento = new Vector3(movimientoHorizontal, 0f, movimientoVertical) * velocidad * 2;

            }
            else
            {
                movimiento = new Vector3(movimientoHorizontal, 0f, movimientoVertical) * velocidad;

            }


            rb.velocity = movimiento;

       



    

    }


   
    public void DetectarBola()
    {

        //if (Input.GetKeyDown(KeyCode.Joystick2Button0) && Dashing.contStun > 2)
        //{
        //    rb.isKinematic = false;
        //}

        if (Input.GetKeyDown(KeyCode.Joystick1Button0) && tocandoBolaBlanca)
        {
            ajustoFuerza = false;
            Debug.Log("Catch");

            if (SceneManager.GetSceneByName("Futpool") == SceneManager.GetActiveScene())
            {
                //tC.UpdateTrajectory();
            }

            //Comienza a dar vueltas
            if (Catch)
            {
                holdingball = false;
                Catch = false;
                throwingBall = true;

                //Debug.Log("Hola");


                //ballRB = Instantiate(ballRB, apuntar.transform.position, Quaternion.identity);

                //if (throwingBall)
                //{




                //ballRB.AddForce(rb.transform.forward * ballForce, ForceMode.Impulse);
                //if (ajustoFuerza)
                //{
                //normalCamara.SetActive(true);
                //shootCamara.SetActive(false);
                //if (SceneManager.GetSceneByName("Futpool") == SceneManager.GetActiveScene())
                //{
                //    tC.Disparar();
                //}

                rb.isKinematic = false;
                VictoryCondition.ContTimesPatadasAmarillo--;
                TrajectoryController.disparoLaPelotaJ1 = true;
                //Cansado.SetActive(true);
                //Slow();



                //velocidad -= 3.5f;

                //}
                //else
                //{
                //    //frenar el giro de la trayectoria y aparece el metodo de ajustar la fuerza.
                //   //TrajectoryController.forceAngle = 0;
                //}





                //throwingBall = false;
                //}

            }
            else
            {
                

                holdingball = true;

                Catch = true;
            }
            


        }
    }

    //public void Slow()
    //{
    //    velocidad -= 3.5f;

    //}

    public void CatchBall()
    {
        if (Catch && SceneManager.GetSceneByName("Futpool") == SceneManager.GetActiveScene())
        {

            pataPateando.SetActive(true);

            if (tocandoBolaBlanca)
            {
                rb.isKinematic = true;
            }
            else
            {
                rb.isKinematic = false;
                Catch = false;
            }

            ballRB.velocity *= 0.1f;

            //timerRotation += Time.deltaTime;

            //transform.Rotate(new Vector3(0f, 100f, 0f) * Time.deltaTime);

            //if (transform.rotation.eulerAngles.y < 150 && RotoTodo == false)
            //{
            //    transform.Rotate(new Vector3(0f, 100f, 0f) * Time.deltaTime);
            //}
            //else
            //{
            //    RotoTodo = true;
            //    transform.Rotate(new Vector3(0f, -100f, 0f) * Time.deltaTime);

            //    if (transform.rotation.eulerAngles.y < 50)
            //    {
            //        RotoTodo = false;
            //    }

            //}




        }
        else
        {

            //if (Dashing.barriendo && DashingP2.Escudo2 && Dashing.tocandoAlOtroPlayer)
            //{
            //    rb.isKinematic = true;
            //}
            //else { }
            pataPateando.SetActive(false);
            if (DashingP2.PJ2StuneoAPJ)
            {
           
                //rb.isKinematic = true; //ACA HACE A UNO PROPIO STUNEARSE
                Debug.Log("ESTOY STUNEADO");
                //Mareado.SetActive(true);
            }
            else
            {

                //Mareado.SetActive(false);
                //rb.isKinematic = false;
                //if (!DashingP2.Escudo2)
                //{
                //    rb.isKinematic = false;
                //}

            }
           
            //transform.Rotate(new Vector3(0f, 0, 0f));
        }
    }


    //public void ThrowBall()
    //{

    //}

    //private void GroundedCheck()
    //{
    //    // set sphere position, with offset
    //    Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z);
    //    Grounded = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers, QueryTriggerInteraction.Ignore);
    //}


    //private void JumpAndGravity()
    //{
    //    if (Grounded)
    //    {
    //        // reset the fall timeout timer
    //        _fallTimeoutDelta = FallTimeout;

    //        // stop our velocity dropping infinitely when grounded
    //        if (_verticalVelocity < 0.0f)
    //        {
    //            _verticalVelocity = -2f;
    //        }

    //        // Jump
    //        if (_input.jump && _jumpTimeoutDelta <= 0.0f)
    //        {
    //            // the square root of H * -2 * G = how much velocity needed to reach desired height
    //            _verticalVelocity = Mathf.Sqrt(JumpHeight * -2f * Gravity);
    //        }

    //        // jump timeout
    //        if (_jumpTimeoutDelta >= 0.0f)
    //        {
    //            _jumpTimeoutDelta -= Time.deltaTime;
    //        }
    //    }
    //    else
    //    {
    //        // reset the jump timeout timer
    //        _jumpTimeoutDelta = JumpTimeout;

    //        // fall timeout
    //        if (_fallTimeoutDelta >= 0.0f)
    //        {
    //            _fallTimeoutDelta -= Time.deltaTime;
    //        }

    //        // if we are not grounded, do not jump
    //        _input.jump = false;
    //    }

    //    // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
    //    if (_verticalVelocity < _terminalVelocity)
    //    {
    //        _verticalVelocity += Gravity * Time.deltaTime;
    //    }
    //}

    //public void Barrer()
    //{

    //}

    //public void Cubrirse()
    //{

    //}

    //public void Stuneado()
    //{

    //}

    //public void CamaraLentaCuandoVanAPatear()
    //{

    //}



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ball")
        {
            Debug.Log("TOCANDO LA BOLA BLANCAAAA");
            tocandoBolaBlanca = true;
            rbBolaBlanca_2 = other.gameObject.GetComponent<Rigidbody>();
        }

        if (other.gameObject.tag == "Bola")
        {
            tocandoBolaColor = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Ball")
        {
            tocandoBolaBlanca = false;
            rb.isKinematic = false;
        }

        if (other.gameObject.tag == "Bola")
        {
            tocandoBolaColor = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Bola")
        {
            tocandoBolaColor = true;
        }
        //else
        //{
        //    tocandoBolaColor = false;
        //}
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bola")
        {
            tocandoBolaColor = true;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Bola")
        {
            tocandoBolaColor = true;

        }
    }

}
