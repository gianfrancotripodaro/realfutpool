using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatesMachineController : MonoBehaviour
{
    public Transform jugadorP1;
    public Transform pelota;
    public Transform arcoP1;
    public Transform arcoIA;


    public float distanciaJugadorP1;
    public float distanciaPelota;
    public float distanciaArcoP1;
    public float distanciaArcoIA;


    public Vector3 puntoInicial;

    public Animator StateMachine;

    public Rigidbody Rbball;

    public float contLlevarPelota; 

    //private Player2 PlayerIA;


    private void Start()
    {
        //PlayerIA = gameObject.GetComponent<Player2>();
        //animator.GetComponent<Animator>();
        puntoInicial = transform.position;

        contLlevarPelota = 0;
    }


    private void Update()
    {
        distanciaJugadorP1 = Vector3.Distance(transform.position, jugadorP1.position);
        distanciaPelota = Vector3.Distance(transform.position, pelota.position);
        distanciaArcoIA = Vector3.Distance(transform.position, arcoIA.position);
        distanciaArcoP1 = Vector3.Distance(transform.position, arcoP1.position);

        StateMachine.SetFloat("DistanciaJugadorP1", distanciaJugadorP1);
        StateMachine.SetFloat("DistanciaPelota", distanciaPelota);
        StateMachine.SetFloat("DistanciaArcoIA", distanciaArcoIA);
        StateMachine.SetFloat("DistanciaArcoP1", distanciaArcoP1);

        contLlevarPelota += Time.deltaTime;
    }

    private void FixedUpdate()
    {
        DespejarPelota();
        IrLlevandoseLaPelota();
    }



    public void MirarPelota()
    {
        if (Player2.tocandoBolaBlancaP2 && pelota.transform.position.x < 1)
        {
            gameObject.transform.LookAt(arcoP1.position);
        }
        if (pelota.transform.position.y > 2 )
        {
            gameObject.transform.LookAt(BuscarPelotaComportamiento.PJIA.transform.position);
        }
        else
        {
            gameObject.transform.LookAt(pelota.transform.position);
        }
    }

    public void IrLlevandoseLaPelota()
    {

        if (Player2.tocandoBolaBlancaP2 && contLlevarPelota > 2)
        {

            Rbball.isKinematic = true;
            Rbball.isKinematic = false;
            Rbball.AddForce(arcoP1.transform.position * 80f, ForceMode.Force);
            TutorialController.contLlevarseLaBola++;
            contLlevarPelota = 0;
        }

    }


    public void PatearPelota()
    {
        //Debug.Log("No entra del todo");
        if (Player2.tocandoBolaBlancaP2)
        {
           
            transform.LookAt(arcoP1.position);
            Rbball.AddForce(transform.forward.normalized * 230f * Time.deltaTime, ForceMode.Impulse);
        }
    
    }

    public void DespejarPelota()
    {
        if (Player2.tocandoBolaBlancaP2 && distanciaArcoIA < 4)
        {
            //despejar pelota
            Rbball.AddForce(transform.transform.up * 0.2f , ForceMode.VelocityChange);
            Rbball.AddForce(/*Player2.movimiento*/ arcoP1.transform.position * 0.2f, ForceMode.Impulse);
        }
    }

    public void MirarArcoIA()
    {

    }

    public void MirarArcoP1()
    {

    }





}
