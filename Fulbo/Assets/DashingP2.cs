using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class DashingP2 : MonoBehaviour
{
    public Rigidbody rb;
    public Transform orientation;
    public float dashForce;
    public float dashTime;

    public static bool barriendo;

    //public Animator animBarrer;

    public Collider ColliderPlayer;

    public Rigidbody otroPlayer;

    public static bool PJ2StuneoAPJ;

    public static float contStun;

    public static bool Escudo2;

    public GameObject objEscudo;

    public static bool hiceParry;

    public float enfriamientoDash;

    public bool YaBarri;

    //public int CargasDash;

    public int TotalDashes = 3;

    public static int DashesDisponibles;

    public bool tocaPared;


    public Vector3 DirIA; /*= new Vector3(1, 0, 0);*/


    public Animator animator;


    public GameObject polvoDashParticulas;

    public static float ContPolvo;

    public static bool soltePolvo;
    //public string barriendoAnim;

    //public GameObject Mareado;


    public Vector3 pPosition;

    public bool instanciarPolvoDash;

    public static bool P2AP1;
    public static bool P2AP3;
    public static bool P2AP4;

    public static bool P2ParryAP1;
    public static bool P2ParryAP3;
    public static bool P2ParryAP4;

    public Transform arcoP1;

    public Rigidbody bola;


    void Start()
    {
        DashesDisponibles = 0;
        barriendo = false;
        DirIA = new Vector3(0.5f, 0, 0);
        enfriamientoDash = -1;
        rb = GetComponent<Rigidbody>();
    }

    public void FixedUpdate()
    {
        //Debug.Log(P2AP3);
        pPosition = this.transform.position + new Vector3(0, -0.2f, 0);

        if (instanciarPolvoDash)
        {
            Instantiate(polvoDashParticulas, pPosition, rb.rotation);
            polvoDashParticulas.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (TutorialController.DasheoAlRival && SceneManager.GetSceneByName("Tutorial") == SceneManager.GetActiveScene())
        {
            if (tocaPared)
            {
                Player2.movimiento = -DirIA;
            }
            else
            {
                Player2.movimiento = DirIA;
            }


            barriendo = true;
            Vector3 forceToApply = Player2.movimiento * dashForce * Time.deltaTime /*+ orientation.up*/;
            rb.AddForce(forceToApply, ForceMode.Impulse);
            //StartCoroutine(Dash());
        }

        if (SceneManager.GetSceneByName("FreeKick") == SceneManager.GetActiveScene())
        {
            if (tocaPared)
            {
                Player2.movimiento = -DirIA;
            }
            else
            {
                Player2.movimiento = DirIA;
            }


            barriendo = true;
            Vector3 forceToApply = Player2.movimiento * dashForce * Time.deltaTime /*+ orientation.up*/;
            rb.AddForce(forceToApply, ForceMode.Impulse);

            //apuntar y disparar al arco

            if (Player2.tocandoBolaBlancaP2)
            {
                gameObject.transform.LookAt(arcoP1.position);
                bola.AddForce(transform.forward.normalized * 2500 * Time.deltaTime , ForceMode.Impulse);
            }



        }

        if (Gamepad.all.Count > 1)
        {


            //Debug.Log(contStun);
            if (soltePolvo)
            {
                ContPolvo += Time.deltaTime;

                if (ContPolvo > 1)
                {
                    //polvoDashParticulas.SetActive(false);
                    //Destroy(GameObject.FindGameObjectWithTag("DashParticulas"));
                    ContPolvo = 0;
                    soltePolvo = false;
                }

            }

            //Debug.Log("TocaPared:" + tocaPared);

          




           



            if (YaBarri)
            {
                if (enfriamientoDash < 0)
                {
                    YaBarri = false;
                }

                enfriamientoDash -= Time.deltaTime;



            }

            if (Escudo2)
            {
                rb.isKinematic = false;
            }

            if (hiceParry)
            {
                contStun += Time.deltaTime;
            }

            if (PJ2StuneoAPJ)
            {
                contStun += Time.deltaTime;
                Physics.IgnoreLayerCollision(6, 6, true);
            }

            if (contStun > 2)
            {
                PJ2StuneoAPJ = false;
                P2AP1 = false;
                P2AP3 = false;
                P2AP4 = false;
                hiceParry = false;
                contStun = 0;
                Physics.IgnoreLayerCollision(6, 6, false);
                otroPlayer.isKinematic = false;
                //Mareado.SetActive(false);
            }



            if (/*Input.GetKeyDown(KeyCode.Joystick2Button4) */  JoystickPlayerSelection.gamepad2.leftShoulder.wasPressedThisFrame && enfriamientoDash < 0 && DashesDisponibles > 0 && !Escudo2)
            {


                enfriamientoDash = 0.4f /*1.5f*/;
                YaBarri = true;
                StartCoroutine(Dash());

            }


            if (/*Input.GetKeyDown(KeyCode.Joystick2Button5)*/  JoystickPlayerSelection.gamepad2.rightShoulder.wasPressedThisFrame && !barriendo)
            {
                StartCoroutine(Cubriendo());
            }

        }
    }

    IEnumerator Dash()
    {
        float startTime = Time.time;
        DashesDisponibles--;

        soltePolvo = true;

        //animBarrer.Play("Barrer");

        while (Time.time < startTime + dashTime)
        {
            //Escudo2 = false;
            //objEscudo.SetActive(false);

            if (Player2.tocandoBolaColor)
            {
                ColliderPlayer.isTrigger = true;
            }
            else
            {
                //ColliderPlayer.isTrigger = false;
            }
            instanciarPolvoDash = true;

            animator.SetBool("Barriendo", true);
            //animBarrer.enabled = true;
            barriendo = true;
            Vector3 forceToApply = Player2.movimiento * dashForce * Time.deltaTime /*+ orientation.up*/;
            rb.AddForce(forceToApply, ForceMode.Impulse);

            yield return null;
        }
        instanciarPolvoDash = false;
        animator.SetBool("Barriendo", false);
        barriendo = false;
        ColliderPlayer.isTrigger = true;
        
        //animBarrer.("Barrer");
        //animBarrer.enabled = false;
        //animBarrer.enabled = false;

    }

    IEnumerator Cubriendo()
    {
        float startTime = Time.time;
        while (Time.time < startTime + 0.5)
        {
            objEscudo.SetActive(true);
            //Mareado.SetActive(false);
            Escudo2 = true; //TENGO QUE HACERLO FALSE
            yield return null;
            rb.velocity *= 0.7f;
        }
        Escudo2 = false;
        objEscudo.SetActive(false);
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.tag == "Player")
    //    {
    //        Destroy(other);
    //        if (barriendo)
    //        {
    //            Debug.Log("Stun");
    //            //otroPlayer  = other.gameObject.GetComponent<Rigidbody>();
    //            Destroy(other);
    //        }
    //        //otroPlayer.isKinematic = true;
    //        //tocandoBolaColor = true;
    //    }
    //}

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Wall")
        {
            //DirIA = -DirIA;
            //DirIA = new Vector3(-1, 0, 0);
            if (tocaPared)
            {
                tocaPared = false;
            }
            else
            {
                tocaPared = true;
            }
            

            //if (tocaPared)
            //{

            //    //Player2.movimiento = -DirIA;
            //    tocaPared = false;
            //}
            //else
            //{

            //    //Player2.movimiento = DirIA;
            //    tocaPared = true;
            //}

            Debug.Log("TocoPared");
        }

        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("FuncionaP2");
            //Destroy(collision.gameObject);

            //otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
            //otroPlayer.velocity *= 0.5f;


            //if (barriendo)
            //{
            //    Debug.Log("Stun");
            //    otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
            //    otroPlayer.isKinematic = true;
            //    Stuneado2 = true;

            //    Physics.IgnoreLayerCollision(6, 6, true);
            //    //otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
            //    //Destroy(collision.gameObject);
            //    //otroPlayer.isKinematic = true;
            //}

           


            if (barriendo /*&& !Escudo2*/)
            {
                Debug.Log("SoyElplayer2");
                otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
                otroPlayer.isKinematic = true;

                PJ2StuneoAPJ = true;



                if (otroPlayer.name == "Player")
                {
                    //DashingP2.Stuneado2 = true;
                    P2AP1 = true;
                    Debug.Log("Fue Stuneado el pj1 por el pj2");
                }

                if (otroPlayer.name == "Player 3")
                {
                    P2AP3 = true;
                    Debug.Log("Fue Stuneado el pj3 por el pj2");
                }

                if (otroPlayer.name == "Player 4")
                {
                    P2AP4 = true;
                    Debug.Log("Fue Stuneado el pj4 por el pj2");
                }


                if (VictoryCondition.ContTimesPatadasAmarillo < 2)
                {
                    VictoryCondition.PateaTeam = 2;
                }
                else
                {
                    VictoryCondition.ContTimesPatadasAmarillo--;
                }



                //Physics.IgnoreLayerCollision(6, 6,true);
                //otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
                //Destroy(collision.gameObject);
                //otroPlayer.isKinematic = true;
            }
            //else
            //{

                if (/*Dashing.barriendo  &&*/ Escudo2)
                {
                    otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
                    otroPlayer.isKinematic = true;
                    rb.isKinematic = false;
                //Stuneado2 = true;
                //rb.isKinematic = true;
                //Debug.Log("Estuneo al otro");

                if (otroPlayer.name == "Player")
                {
                    //DashingP2.Stuneado2 = true;
                    Debug.Log("Se le devolvio Stuneado el pj1 por el pj2");
                    P2ParryAP1 = true;
                }

                if (otroPlayer.name == "Player 3")
                {
                    Debug.Log("Se le devolvio Stuneado el pj3 por el pj2");
                    P2ParryAP3 = true;
                }

                if (otroPlayer.name == "Player 4")
                {
                    Debug.Log("Se le devolvio Stuneado el pj4 por el pj2");
                    P2ParryAP4 = true;
                }


                //Mareado.SetActive(true);
                hiceParry = true;
                    VictoryCondition.patadasTeamVioletax2 = true;

                    VictoryCondition.PateaTeam = 2;
                    VictoryCondition.ContTimesPatadasVioloeta = 2;
                    VictoryCondition.patadasTeamAmarillox2 = false;

                }
            //else if (/*Dashing.barriendo */ DashingP3.barriendo && Escudo2)
            //{
            //    otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
            //    otroPlayer.isKinematic = true;
            //    rb.isKinematic = false;
            //    //Stuneado2 = true;
            //    //rb.isKinematic = true;
            //    Debug.Log("Estuneo al otro");
            //    //Mareado.SetActive(true);
            //    hiceParry = true;
            //    VictoryCondition.patadasTeamVioletax2 = true;

            //    VictoryCondition.PateaTeam = 2;
            //    VictoryCondition.ContTimesPatadasVioloeta = 2;
            //    VictoryCondition.patadasTeamAmarillox2 = false;

            //}
            //{

            //}



         


            //Physics.IgnoreLayerCollision(6, 6, false);

            //tocandoBolaColor = true;
        }
    }
}
