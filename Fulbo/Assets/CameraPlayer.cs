using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayer : MonoBehaviour
{
    private Vector2 angle = new Vector2(-90 * Mathf.Deg2Rad, 0);

    public Transform follow;
    public float distance;
    float hor;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Player2.Catch)
        {
             hor = Input.GetAxis("horizontalMando2");
        }

        if (PlayerMovement.Catch)
        {
             hor = Input.GetAxis("Horizontal");
        }
      

        if (hor !=0)
        {
            angle.x += hor * Mathf.Deg2Rad;

        }
    }

    private void LateUpdate()
    {

        Vector3 orbit = new Vector3(Mathf.Cos(angle.x), 0, Mathf.Sin(angle.x));

        transform.position = follow.position + orbit * distance;
        transform.rotation = Quaternion.LookRotation(follow.position - transform.position);
    }
}
