using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashingP4 : MonoBehaviour
{
    public Rigidbody rb;
    public Transform orientation;
    public float dashForce;
    public float dashTime;

    public static bool barriendo;

    //public Animator animBarrer;

    public Collider ColliderPlayer;

    public Rigidbody otroPlayer;

    public static bool PJ4StuneoAPJ;

    public static float contStun;


    public static bool Escudo;

    public GameObject objEscudo;

    public static bool tocandoAlOtroPlayer;

    public static bool hiceParry;

    public float enfriamientoDash;

    public bool YaBarri;

    public int TotalDashes = 3;

    public static int DashesDisponibles;


    public Animator animator;

    public GameObject polvoDashParticulas;

    public static float ContPolvo;

    public static bool soltePolvo;
    //public string barriendoAnim;

    //public GameObject Mareado;


    public Vector3 pPosition;

    public bool instanciarPolvoDash;


    public static bool P4AP1;
    public static bool P4AP2;
    public static bool P4AP3;

    public static bool P4ParryAP1;
    public static bool P4ParryAP2;
    public static bool P4ParryAP3;

    void Start()
    {
        DashesDisponibles = 0;
        enfriamientoDash = -1;
        rb = GetComponent<Rigidbody>();
    }

    public void FixedUpdate()
    {
        pPosition = this.transform.position + new Vector3(0, -0.2f, 0);

        if (instanciarPolvoDash)
        {
            Instantiate(polvoDashParticulas, pPosition, rb.rotation);
            polvoDashParticulas.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (soltePolvo)
        {
            ContPolvo += Time.deltaTime;

            if (ContPolvo > 1)
            {
                //polvoDashParticulas.SetActive(false);
                //Destroy(GameObject.FindGameObjectWithTag("DashParticulas"));
                ContPolvo = 0;
                soltePolvo = false;
            }

        }




        if (YaBarri)
        {


            if (enfriamientoDash < 0)
            {
                YaBarri = false;
            }

            enfriamientoDash -= Time.deltaTime;



        }

        if (Escudo)
        {
            rb.isKinematic = false;

        }

        if (hiceParry)
        {
            contStun += Time.deltaTime;

        }

        if (PJ4StuneoAPJ)
        {
            contStun += Time.deltaTime;
            Physics.IgnoreLayerCollision(6, 6, true);
        }

        if (contStun > 2)
        {
            PJ4StuneoAPJ = false;
            hiceParry = false;
            P4AP2 = false;
            P4AP3 = false;
            P4AP1 = false;
            P4ParryAP1 = false;
            P4ParryAP2 = false;
            P4ParryAP3 = false;
            contStun = 0;
            Physics.IgnoreLayerCollision(6, 6, false);
            otroPlayer.isKinematic = false;
            //Mareado.SetActive(false);
        }

        //if ()
        //{

        //}



        if (JoystickPlayerSelection.gamepad4.leftShoulder.wasPressedThisFrame && enfriamientoDash < 0 && DashesDisponibles > 0 && !Escudo)
        {
            enfriamientoDash = 0.8f /*1.5f*/;
            YaBarri = true;

            StartCoroutine(Dash());

        }

        if (JoystickPlayerSelection.gamepad4.rightShoulder.wasPressedThisFrame && !barriendo)
        {
            StartCoroutine(Cubriendo());
        }


    }


    IEnumerator Dash()
    {
        float startTime = Time.time;
        DashesDisponibles--;

        soltePolvo = true;

        //animBarrer.Play("Barrer");

        while (Time.time < startTime + dashTime)
        {


            if (PlayerMovement.tocandoBolaColor)
            {
                ColliderPlayer.isTrigger = true;
            }
            else
            {
                //ColliderPlayer.isTrigger = false;
            }
            instanciarPolvoDash = true;

            //Instantiate(polvoDashParticulas, this.transform);

            animator.SetBool("Barriendo", true);
            //animator.SetBool( );
            //animBarrer.enabled = true;
            barriendo = true;
            Vector3 forceToApply = PlayerMovement.movimiento * dashForce * Time.deltaTime /*+ orientation.up*/;
            rb.AddForce(forceToApply, ForceMode.Impulse);

            yield return null;
        }
        instanciarPolvoDash = false;
        animator.SetBool("Barriendo", false);
        barriendo = false;
        ColliderPlayer.isTrigger = true;

        //animBarrer.("Barrer");
        //animBarrer.enabled = false;
        //animBarrer.enabled = false;



    }



    IEnumerator Cubriendo()
    {
        float startTime = Time.time;
        while (Time.time < startTime + 0.5)
        {
            objEscudo.SetActive(true);
            Escudo = true;  //TENGO QUE HACERLO FALSE
            //Mareado.SetActive(false);
            yield return null;
            rb.velocity *= 0.7f;
        }
        Escudo = false;
        //PlayerMovement.Parry = false;
        objEscudo.SetActive(false);
    }
    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.tag == "Player")
    //    {
    //        Destroy(other);
    //        if (barriendo)
    //        {
    //            Debug.Log("Stun");
    //            //otroPlayer  = other.gameObject.GetComponent<Rigidbody>();
    //            Destroy(other);
    //        }
    //        //otroPlayer.isKinematic = true;
    //        //tocandoBolaColor = true;
    //    }
    //}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("FuncionaP1");
            //Destroy(collision.gameObject);

            //otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
            //otroPlayer.velocity *= 0.5f;
            //if (DashingP2.barriendo && Escudo)
            //{
            //    otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
            //    otroPlayer.isKinematic = true;
            //    Debug.Log("Estuneo al otro");
            //}
            //else
            //{

            //}

            //if (PlayerMovement.Parry)
            //{
            //    otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
            //    otroPlayer.isKinematic = true;
            //}




            if (barriendo /*&& !Escudo*/)
            {
                TutorialController.DasheoAlRival = true;

                //Debug.Log("SoyElplayer1");
                otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
                otroPlayer.isKinematic = true;

                PJ4StuneoAPJ = true;

                //if (otroPlayer.name == "Player1")
                //{

                //}

                if (otroPlayer.name == "Player 2")
                {
                    P4AP2 = true;
                    //DashingP2.Stuneado2 = true;
                    Debug.Log("Fue Stuneado el pj2 por el pj4");
                }

                if (otroPlayer.name == "Player 3")
                {
                    P4AP3 = true;
                    Debug.Log("Fue Stuneado el pj3 por el pj4");
                }

                if (otroPlayer.name == "Player")
                {
                    P4AP1 = true;
                    Debug.Log("Fue Stuneado el pj1 por el pj4");
                }





                if (VictoryCondition.ContTimesPatadasVioloeta < 2)
                {
                    VictoryCondition.PateaTeam = 1;
                }
                else
                {
                    VictoryCondition.ContTimesPatadasVioloeta--;
                }


                //Physics.IgnoreLayerCollision(6, 6,true);
                //otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
                //Destroy(collision.gameObject);
                //otroPlayer.isKinematic = true;
            }
            //else
            //{
            if (Escudo)
            {
                TutorialController.StuneeConUnParry = true;



                otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
                otroPlayer.isKinematic = true;
                rb.isKinematic = false;
                //Stuneado = true;
                //rb.isKinematic = true;
                //Mareado.SetActive(true);
                if (otroPlayer.name == "Player 2")
                {
                    //DashingP2.Stuneado2 = true;
                    Debug.Log("Se le devolvio Stuneado el pj2 por el pj4");
                    P4ParryAP2 = true;
                }

                if (otroPlayer.name == "Player 3")
                {
                    Debug.Log("Se le devolvio Stuneado el pj3 por el pj4");
                    P4ParryAP3 = true;
                }

                if (otroPlayer.name == "Player")
                {
                    Debug.Log("Se le devolvio Stuneado el pj1 por el pj4");
                    P4ParryAP1 = true;
                }
                hiceParry = true;
                VictoryCondition.patadasTeamAmarillox2 = true;
                //patea 2 veces
                VictoryCondition.PateaTeam = 1;
                VictoryCondition.ContTimesPatadasAmarillo = 2;
                VictoryCondition.patadasTeamVioletax2 = false;
            }
            //}


            //Physics.IgnoreLayerCollision(6, 6, false);


            //tocandoBolaColor = true;
        }
    }
}
