﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSelection:MonoBehaviour {

	[HideInInspector]
	public Rigidbody activeBody;

	List<BallController> allBalls;
	int clickedId = 0;
	bool isMouseDown = false;

	public RaycastHit hit;
	public Ray ray;

    public Rigidbody rbBolaBlanca;

    /**************************************
	 *	ACTIONS
	 */

    void ToggleBall(Rigidbody rigidbody, bool enable = true) {
		if (rigidbody == null)
			return;

		// reset the table so the new trajectory reflects the active scene
		foreach (BallController ball in allBalls) {
			ball.Reset();
		}

		BallController selected = (BallController)rigidbody.gameObject.GetComponent("BallController");
		if (enable) selected.OnSelect();
		else selected.OnDeselect();
	}


	/**************************************
	 *	UNITY CALLBACKS
	 */

	public void Start() {
		// with the scenes setup properly in TrajectoryController.Awake, we can start everything else
		allBalls = new List<BallController>();
		GameObject[] all = GameObject.FindGameObjectsWithTag("Ball");
		foreach (GameObject b in all) {
			allBalls.Add(b.GetComponent<BallController>());
		}
		// select the first ball on the table so there's always something to see
		activeBody = allBalls[0].GetComponent<Rigidbody>();
		ToggleBall(activeBody);
	}

	// whenever the player clicks down AND up on the same ball (ie doesn't move mouse to orbit camera) then select that ball
	void Update() {

       
        Player2.rbBolaBlanca_ = rbBolaBlanca;
        PlayerMovement.rbBolaBlanca_2 = rbBolaBlanca;
        //if (!PlayerMovement.tocandoBolaBlanca)
        //{
        //	if (activeBody)
        //		ToggleBall(activeBody, false);
        //      }
        //      else
        //      {
        //	activeBody = hit.rigidbody;
        //	ToggleBall(activeBody);
        //}


        //ToggleBall(activeBody, false);

        // select the new one

        //if (Input.GetKeyDown(KeyCode.Joystick1Button0))
        //{
        //    ToggleBall(activeBody, false);

        //    select the new one
        //    activeBody = hit.rigidbody;
        //    ToggleBall(activeBody);
        //}



        if (Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            isMouseDown = true;

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                clickedId = rbBolaBlanca.GetInstanceID();
            }
        }
        else if (isMouseDown && Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            isMouseDown = false;

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (clickedId == rbBolaBlanca.GetInstanceID())
                {
                    // deselect current ball
                    if (activeBody)
                        ToggleBall(activeBody, false);

                    // select the new one
                    activeBody = hit.rigidbody;
                    ToggleBall(activeBody);
                }
            }
        }


        //if (Input.GetKeyDown(KeyCode.Joystick1Button0))
        //{
        //    ToggleBall(activeBody, false);

        //    // select the new one
        //    activeBody = hit.rigidbody;
        //    ToggleBall(activeBody);
        //}
    }



    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.tag == "Ball")
    //    {
    //        rbBolaBlanca = other.gameObject.GetComponent<Rigidbody>();
    //    }

    //}


}
