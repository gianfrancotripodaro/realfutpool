﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TrajectoryController:MonoBehaviour {

	[HideInInspector]
	public static string hiddenSceneName = "TrajectoryScene";
	[HideInInspector]
	public Vector3 applyForce = new Vector3();

	[Range(0.0f, 80.0f)]
	public float horizontalForce = 10f;
	[Range(0.0f, 30.0f)]
	public float verticalForce = 0f;
	public static float forceAngle = 1.58f;
	public float physicsTimescale = 1f;
	[Range(50, 500)]
	public int simIterations = 250;

	float prevHorForce = 0f;
	float prevVertForce = 0f;
	float prevAngle = 0f;

	BallSelection selection;
	List<BallController> allBalls;
	Scene mainScene;
	Scene trajectoryScene;
	PhysicsScene trajectoryPhysicsScene;
	PhysicsScene mainScenePhysics;

	//public GameObject Player;

	public static bool ListoDisparar;

	public static bool disparoLaPelotaJ1;
	public static bool disparoLaPelotaJ2;
	public static bool disparoLaPelotaJ3;

	/**************************************
	 *	ACTIONS
	 */


	public void LateUpdate()
    {
		/*if (Input.GetMouseButtonDown(0))*/ //ACA PONGO QUE EL JUGADOR CUANDO TOCA LA PELOTA Y HACE CLICK APAREZCA LA TRAYECTORIA YA PARA QUE DE VUELTAS Y DESPUES PARA VER SI LE PEGA HACIA ARRIBA O HACIA ABAJO, Y EN LA SIGUIENTE CALCULA LA FUERZA CON UNA BARRA Y YA LA TIRA.
		//{
			if (forceAngle != prevAngle || horizontalForce != prevHorForce || verticalForce != prevVertForce &&(PlayerMovement.tocandoBolaBlanca))
			{
				// the user changed something in the inspector, so update the sim

				UpdateTrajectory();
				//UpdateTrajectory();
			}


		//}

	}

  public  void UpdateTrajectory() {
		//if (PlayerMovement.tocandoBolaBlanca)
		//{

			// reset the (hidden) scene to match the active scene (the table never moves so only worry about the balls)
			foreach (BallController ball in allBalls)
			{
				ball.Reset();
			}
			// update the force we'll apply to the active ball
			applyForce.Set(horizontalForce * Mathf.Cos(forceAngle), verticalForce, horizontalForce * Mathf.Sin(forceAngle));
			prevAngle = forceAngle;
			prevHorForce = horizontalForce;
			prevVertForce = verticalForce;
			// make sure the scenes are valid and a ball is selected
			if (!mainScenePhysics.IsValid() || !trajectoryPhysicsScene.IsValid() || selection.activeBody == null)
				return;

			BallController bc = selection.activeBody.gameObject.GetComponent<BallController>();
			bc.UpdateTrajectory(applyForce, simIterations, physicsTimescale);
		//}
        //else
        //{

        //}
	}


	/**************************************
	 *	UNITY CALLBACKS
	 */

	void Awake() {
		Physics.autoSimulation = false; // this means we have to run the physics scene(s) manually now in our own FixedUpdate
		mainScene = SceneManager.GetActiveScene();
		mainScenePhysics = mainScene.GetPhysicsScene();

		// you must create a scene with the LocalPhysicsMode.Physics3D parameter or the Simulate (float time) method for the trajectoryScene scene will simulate physics for all physical scenes
		CreateSceneParameters sceneParam = new CreateSceneParameters(LocalPhysicsMode.Physics3D);
		trajectoryScene = SceneManager.CreateScene(hiddenSceneName, sceneParam);
		trajectoryPhysicsScene = trajectoryScene.GetPhysicsScene();

		selection = GetComponent<BallSelection>();
		allBalls = new List<BallController>();
		GameObject[] all = GameObject.FindGameObjectsWithTag("Ball");
		foreach (GameObject b in all) {
			allBalls.Add(b.GetComponent<BallController>());
		}
	}

	void FixedUpdate() {
		if (!mainScenePhysics.IsValid())
			return;

		mainScenePhysics.Simulate(Time.fixedDeltaTime * physicsTimescale);


		//if (PlayerMovement.Catch)
		//{

		//	forceAngle += 0.05f;
		//}

		//AjustarTrajectoria();
		AjustarTrajectoria();
	}

	void Update() {

		
		//ACA TENGO LA TRAYECTORIA HORIZONTAL

		//      if (Player2.tocandoBolaBlancaP2)
		//      {
		//	Player = GameObject.FindWithTag("Player2");
		//      }

		//      if (PlayerMovement.tocandoBolaBlanca)
		//      {
		//	Player = GameObject.FindWithTag("Player");
		//}

		//TENGO QUE AGARRAR LA FUERZA Y LA TRAYECTORIA VERTICAL


	}


	public void Disparar()
    {
		//if (Input.GetKeyDown(KeyCode.Joystick1Button0))
	///*	{  //AC*/A LA DISPARO

	//		PlayerMovement.holdingball = false;
	//		PlayerMovement.Catch = false;
			selection.activeBody.AddForce(applyForce, ForceMode.Impulse);

        //disparoLaPelotaJ1 = true;

        //disparoLaPelotaJ2 = true;

        //}
    }


	public void AjustarTrajectoria()
    {
		if (PlayerMovement.Catch || Player2.Catch || Player3.Catch)
		{
            if (Player2.tocandoBolaBlancaP2)
            {
				Debug.Log("TENGO QUE APUNTAR");
				if ((Input.GetKey(KeyCode.Joystick2Button2)))
				{
					horizontalForce += 0.5f;
				}

				if ((Input.GetKey(KeyCode.Joystick2Button1)))
				{
					horizontalForce -= 0.5f;
				}

				if ((Input.GetKey(KeyCode.Joystick2Button4)))
				{
					//Debug.Log("Aumenta");
					forceAngle += 0.02f;
				}

				if ((Input.GetKey(KeyCode.Joystick2Button5)))
				{
					//Debug.Log("Disminuye");
					forceAngle -= 0.02f;
				}
			}


			if (PlayerMovement.tocandoBolaBlanca)
            {
				Debug.Log("TENGO QUE APUNTAR");
				if ((Input.GetKey(KeyCode.Joystick1Button2)))
				{
					horizontalForce += 0.5f;
				}

				if ((Input.GetKey(KeyCode.Joystick1Button1)))
				{
					horizontalForce -= 0.5f;
				}

				if ((Input.GetKey(KeyCode.Joystick1Button4)))
				{
					//Debug.Log("Aumenta");
					forceAngle += 0.02f;
				}

				if ((Input.GetKey(KeyCode.Joystick1Button5)))
				{
					//Debug.Log("Disminuye");
					forceAngle -= 0.02f;
				}
			}

			if (Player3.tocandoBolaBlanca)
			{
				Debug.Log("TENGO QUE APUNTAR");
				if ((Input.GetKey(KeyCode.Joystick4Button2)))
				{
					horizontalForce += 0.5f;
				}

				if ((Input.GetKey(KeyCode.Joystick4Button1)))
				{
					horizontalForce -= 0.5f;
				}

				if ((Input.GetKey(KeyCode.Joystick4Button4)))
				{
					//Debug.Log("Aumenta");
					forceAngle += 0.02f;
				}

				if ((Input.GetKey(KeyCode.Joystick4Button5)))
				{
					//Debug.Log("Disminuye");
					forceAngle -= 0.02f;
				}
			}


			float joystickY = Input.GetAxis("Vertical2");

			if (joystickY > 0.2f)
			{
				verticalForce += 0.5f;

			}

			if (joystickY < -0.2f)
			{
				verticalForce -= 0.5f;
			}

		}
	}
}
