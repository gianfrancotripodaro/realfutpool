using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoyoTrigger : MonoBehaviour
{
    public GameObject Paredes;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bola" /*|| other.gameObject.tag == "Ball"*/)
        {
            Debug.Log("Entra");
            Paredes.SetActive(false);
        }
        else
        {
            Paredes.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Bola" /*|| other.gameObject.tag == "Ball"*/)
        {
            Debug.Log("Sale");
            Paredes.SetActive(true);
        }
    }
}
