using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DashingP3 : MonoBehaviour
{
    public Rigidbody rb;
    public Transform orientation;
    public float dashForce;
    public float dashTime;

    public static bool barriendo;

    //public Animator animBarrer;

    public Collider ColliderPlayer;

    public Rigidbody otroPlayer;

    public static bool PJ3StuneoAPJ;

    public static float contStun;

    public static bool Escudo2;

    public GameObject objEscudo;

    public static bool hiceParry;

    public float enfriamientoDash;

    public bool YaBarri;

    //public int CargasDash;

    public int TotalDashes = 3;

    public static int DashesDisponibles;

    public bool tocaPared;


    public Vector3 DirIA; /*= new Vector3(1, 0, 0);*/


    public Animator animator;


    public GameObject polvoDashParticulas;

    public static float ContPolvo;

    public static bool soltePolvo;
    //public string barriendoAnim;

    //public GameObject Mareado;


    public Vector3 pPosition;

    public bool instanciarPolvoDash;

    public static bool P3AP1;
    public static bool P3AP2;
    public static bool P3AP4;

    public static bool P3ParryAP1;
    public static bool P3ParryAP2;
    public static bool P3ParryAP4;

    void Start()
    {
        DashesDisponibles = 0;
        barriendo = false;
        DirIA = new Vector3(0.5f, 0, 0);
        enfriamientoDash = -1;
        rb = GetComponent<Rigidbody>();
    }

    public void FixedUpdate()
    {
        pPosition = this.transform.position + new Vector3(0, -0.2f, 0);

        if (instanciarPolvoDash)
        {
            Instantiate(polvoDashParticulas, pPosition, rb.rotation);
            polvoDashParticulas.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (soltePolvo)
        {
            ContPolvo += Time.deltaTime;

            if (ContPolvo > 1)
            {
                //polvoDashParticulas.SetActive(false);
                //Destroy(GameObject.FindGameObjectWithTag("DashParticulas"));
                ContPolvo = 0;
                soltePolvo = false;
            }

        }

        //Debug.Log("TocaPared:" + tocaPared);

        if (TutorialController.DasheoAlRival && SceneManager.GetSceneByName("Tutorial") == SceneManager.GetActiveScene())
        {
            if (tocaPared)
            {
                Player2.movimiento = -DirIA;
            }
            else
            {
                Player2.movimiento = DirIA;
            }


            barriendo = true;
            Vector3 forceToApply = Player2.movimiento * dashForce * Time.deltaTime /*+ orientation.up*/;
            rb.AddForce(forceToApply, ForceMode.Impulse);
            //StartCoroutine(Dash());
        }



        if (YaBarri)
        {
            if (enfriamientoDash < 0)
            {
                YaBarri = false;
            }

            enfriamientoDash -= Time.deltaTime;



        }

        if (Escudo2)
        {
            rb.isKinematic = false;
        }

        if (hiceParry)
        {
            contStun += Time.deltaTime;
        }

        if (PJ3StuneoAPJ)
        {
            contStun += Time.deltaTime;
            Physics.IgnoreLayerCollision(6, 6, true);
        }

        if (contStun > 2)
        {
            PJ3StuneoAPJ = false;
            hiceParry = false;
            P3AP1 = false;
            P3AP2 = false;
            P3AP4 = false;
            P3ParryAP1 = false;
            P3ParryAP2 = false;
            P3ParryAP4 = false;
        
            contStun = 0;
            Physics.IgnoreLayerCollision(6, 6, false);
            otroPlayer.isKinematic = false;
            //Mareado.SetActive(false);
        }



        if (JoystickPlayerSelection.gamepad3.leftShoulder.wasPressedThisFrame && enfriamientoDash < 0 && DashesDisponibles > 0 && !Escudo2)
        {
            enfriamientoDash = 0.4f /*1.5f*/;
            YaBarri = true;
            StartCoroutine(Dash());

        }


        if (JoystickPlayerSelection.gamepad3.rightShoulder.wasPressedThisFrame && !barriendo)
        {
            StartCoroutine(Cubriendo());
        }
    }

    IEnumerator Dash()
    {
        float startTime = Time.time;
        DashesDisponibles--;

        soltePolvo = true;

        //animBarrer.Play("Barrer");

        while (Time.time < startTime + dashTime)
        {
            //Escudo2 = false;
            //objEscudo.SetActive(false);

            if (Player2.tocandoBolaColor)
            {
                ColliderPlayer.isTrigger = true;
            }
            else
            {
                //ColliderPlayer.isTrigger = false;
            }
            instanciarPolvoDash = true;

            animator.SetBool("Barriendo", true);
            //animBarrer.enabled = true;
            barriendo = true;
            Vector3 forceToApply = Player2.movimiento * dashForce * Time.deltaTime /*+ orientation.up*/;
            rb.AddForce(forceToApply, ForceMode.Impulse);

            yield return null;
        }
        instanciarPolvoDash = false;
        animator.SetBool("Barriendo", false);
        barriendo = false;
        ColliderPlayer.isTrigger = true;

        //animBarrer.("Barrer");
        //animBarrer.enabled = false;
        //animBarrer.enabled = false;

    }

    IEnumerator Cubriendo()
    {
        float startTime = Time.time;
        while (Time.time < startTime + 0.5)
        {
            objEscudo.SetActive(true);
            //Mareado.SetActive(false);
            Escudo2 = true; //TENGO QUE HACERLO FALSE
            yield return null;
            rb.velocity *= 0.7f;
        }
        Escudo2 = false;
        objEscudo.SetActive(false);
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.tag == "Player")
    //    {
    //        Destroy(other);
    //        if (barriendo)
    //        {
    //            Debug.Log("Stun");
    //            //otroPlayer  = other.gameObject.GetComponent<Rigidbody>();
    //            Destroy(other);
    //        }
    //        //otroPlayer.isKinematic = true;
    //        //tocandoBolaColor = true;
    //    }
    //}

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Wall")
        {
            //DirIA = -DirIA;
            //DirIA = new Vector3(-1, 0, 0);
            if (tocaPared)
            {
                tocaPared = false;
            }
            else
            {
                tocaPared = true;
            }


            //if (tocaPared)
            //{

            //    //Player2.movimiento = -DirIA;
            //    tocaPared = false;
            //}
            //else
            //{

            //    //Player2.movimiento = DirIA;
            //    tocaPared = true;
            //}

            Debug.Log("TocoPared");
        }

        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("FuncionaP2");
            //Destroy(collision.gameObject);

            //otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
            //otroPlayer.velocity *= 0.5f;


            //if (barriendo)
            //{
            //    Debug.Log("Stun");
            //    otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
            //    otroPlayer.isKinematic = true;
            //    Stuneado2 = true;

            //    Physics.IgnoreLayerCollision(6, 6, true);
            //    //otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
            //    //Destroy(collision.gameObject);
            //    //otroPlayer.isKinematic = true;
            //}




            if (barriendo /*&& !Escudo2*/)
            {
                Debug.Log("SoyElplayer2");
                otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
                otroPlayer.isKinematic = true;

                PJ3StuneoAPJ = true;



                if (otroPlayer.name == "Player")
                {
                    //DashingP2.Stuneado2 = true;
                    P3AP1 = true;
                    Debug.Log("Fue Stuneado el pj1 por el pj3");
                }

                if (otroPlayer.name == "Player 2")
                {
                    P3AP2 = true;
                    Debug.Log("Fue Stuneado el pj2 por el pj3");
                }

                if (otroPlayer.name == "Player 4")
                {
                    P3AP4 = true;
                    Debug.Log("Fue Stuneado el pj4 por el pj3");
                }


                if (VictoryCondition.ContTimesPatadasAmarillo < 2)
                {
                    VictoryCondition.PateaTeam = 2;
                }
                else
                {
                    VictoryCondition.ContTimesPatadasAmarillo--;
                }



                //Physics.IgnoreLayerCollision(6, 6,true);
                //otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
                //Destroy(collision.gameObject);
                //otroPlayer.isKinematic = true;
            }
            //else
            //{

            if (Escudo2)
            {
                otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
                otroPlayer.isKinematic = true;
                rb.isKinematic = false;
                //Stuneado2 = true;
                //rb.isKinematic = true;
                //Debug.Log("Estuneo al otro");

                if (otroPlayer.name == "Player")
                {
                    //DashingP2.Stuneado2 = true;
                    Debug.Log("Se le devolvio Stuneado el pj1 por el pj3");
                    P3ParryAP1 = true;
                }

                if (otroPlayer.name == "Player 2")
                {
                    Debug.Log("Se le devolvio Stuneado el pj2 por el pj3");
                    P3ParryAP2 = true;
                }

                if (otroPlayer.name == "Player 4")
                {
                    Debug.Log("Se le devolvio Stuneado el pj4 por el pj3");
                    P3ParryAP4 = true;
                }


                //Mareado.SetActive(true);
                hiceParry = true;
                VictoryCondition.patadasTeamVioletax2 = true;

                VictoryCondition.PateaTeam = 2;
                VictoryCondition.ContTimesPatadasVioloeta = 2;
                VictoryCondition.patadasTeamAmarillox2 = false;

            }
            //else if (/*Dashing.barriendo */ DashingP3.barriendo && Escudo2)
            //{
            //    otroPlayer = collision.gameObject.GetComponent<Rigidbody>();
            //    otroPlayer.isKinematic = true;
            //    rb.isKinematic = false;
            //    //Stuneado2 = true;
            //    //rb.isKinematic = true;
            //    Debug.Log("Estuneo al otro");
            //    //Mareado.SetActive(true);
            //    hiceParry = true;
            //    VictoryCondition.patadasTeamVioletax2 = true;

            //    VictoryCondition.PateaTeam = 2;
            //    VictoryCondition.ContTimesPatadasVioloeta = 2;
            //    VictoryCondition.patadasTeamAmarillox2 = false;

            //}
            //{

            //}






            //Physics.IgnoreLayerCollision(6, 6, false);

            //tocandoBolaColor = true;
        }
    }

}
